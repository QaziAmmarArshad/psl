//
//  SelectPlayerTVC.swift
//  PSL
//
//  Created by Mudassar on 11/01/2018.
//  Copyright © 2018 HalfTech. All rights reserved.
//

import UIKit

class SelectPlayerTVC: UITableViewCell {

    @IBOutlet weak var chackBox: UIImageView!
    @IBOutlet weak var playerName: UILabel!
    @IBOutlet weak var teamName: UILabel!
    @IBOutlet weak var playerValue: UILabel!
    @IBOutlet weak var selectBtn: UIButton!
    var playerIsSelected = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setData(index: Int, man: SportMan) {
        if man.isSelected {
             chackBox.image = UIImage(named: "checkbox")
        } else {
             chackBox.image = UIImage(named: "checkBoxEmpty")
        }
        playerName.text = man.name
        selectBtn.tag = index
        teamName.text = man.team_name
        playerValue.text = man.budget
        
    }
}
