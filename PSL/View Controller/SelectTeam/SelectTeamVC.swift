//
//  SelectTeamVC.swift
//  PSL
//
//  Created by Mudassar on 11/01/2018.
//  Copyright © 2018 HalfTech. All rights reserved.
//

import UIKit
enum SelectedCategory {
    case bat
    case ball
    case batBall
    case glove
}

class SelectTeamVC: UIViewController {
    // This variable is come from back screen
    var isHaveATeam = false
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var position = 0
    
    //view for setting color
    @IBOutlet weak var batView: UIView!
    @IBOutlet weak var ballView: UIView!
    @IBOutlet weak var batBallView: UIView!
    @IBOutlet weak var glovesView: UIView!
    
    //change label value on every player selected
    @IBOutlet weak var batsManLbl: UILabel!
    @IBOutlet weak var ballerLbl: UILabel!
    @IBOutlet weak var allRounderLbl: UILabel!
    @IBOutlet weak var gloveLbl: UILabel!
    //budgets and transferable lablers
    @IBOutlet weak var transfersLabel: UILabel!
    @IBOutlet weak var budgetLabel: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    
    var refreshControl = UIRefreshControl()
    
    var players = Players()
    var headerText = "Batsman"
    var selectedCategory: SelectedCategory = .bat
    
    var s_batsman = [SportMan]()
    var s_bowler = [SportMan]()
    var s_wicketKeeper = [SportMan]()
    var s_allrounder = [SportMan]()
    var budget = 100
    var transfer = 50
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        //addging left and righ gestur reconizer
        addGesture()
        print(isHaveATeam)
        getSelectedPlayersApi()
        setPullToRefresh()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK - Set Pull To Refreshd -
    func setPullToRefresh() {
        refreshControl.attributedTitle = NSAttributedString(string: " ")
        refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: UIControlEvents.valueChanged)
        self.tableView.addSubview(refreshControl) // not required when using UITableViewController
    }
    
    //MARK: - Gesture Reconizer Function -
    func addGesture() {
        let leftSwip = UISwipeGestureRecognizer(target: self, action: #selector(handleTap(sender:)))
        let rightSwip = UISwipeGestureRecognizer(target: self, action: #selector(handleTap(sender:)))
        leftSwip.direction = UISwipeGestureRecognizerDirection.left
        rightSwip.direction = UISwipeGestureRecognizerDirection.right
        self.tableView.addGestureRecognizer(leftSwip)
        self.tableView.addGestureRecognizer(rightSwip)
    }
    @objc func handleTap(sender: UISwipeGestureRecognizer) {
        switch sender.direction.rawValue {
        case 2:
            //right gesture
            if position < 4 {  position = (position + 1) }
            changeView(selectedPostion: position)
        case 1:
            //left gesture
            if position > 0 {  position = (position - 1) }
            changeView(selectedPostion: position)
        default:
            break
        }
    }
    
    func changeView(selectedPostion: Int) {
        switch selectedPostion {
        case 0:
            //load ball view
            selectedCategory = .bat
            changeColorOfSelectdView(selectedView: selectedCategory)
        case 1:
            //load ball view
            selectedCategory = .ball
            changeColorOfSelectdView(selectedView: selectedCategory)
        case 2:
            //load allrounder view
            selectedCategory = .batBall
            changeColorOfSelectdView(selectedView: selectedCategory)
        case 3:
            //load wicket Keeper view
            selectedCategory = .glove
            changeColorOfSelectdView(selectedView: selectedCategory)
        default:
            print(selectedPostion)
        }
    }
    
    //MARK: - Button Actions -
    @IBAction func selectBat(_ sender: Any) {
        selectedCategory = .bat
        changeColorOfSelectdView(selectedView: selectedCategory)
    }
    @IBAction func selectBall(_ sender: Any) {
        selectedCategory = .ball
        changeColorOfSelectdView(selectedView: selectedCategory)
    }
    @IBAction func selectBatBall(_ sender: Any) {
        selectedCategory = .batBall
        changeColorOfSelectdView(selectedView: selectedCategory)
    }
    @IBAction func selectGlove(_ sender: Any) {
        selectedCategory = .glove
        changeColorOfSelectdView(selectedView: selectedCategory)
    }
    
    @IBAction func conformButton(_ sender: Any) {
        // This condition check that if total playres are selected
        let totalPlayers = s_bowler + s_batsman + s_allrounder + s_wicketKeeper
        if totalPlayers.count == 11 {
            let selectCapVC = storyboard?.instantiateViewController(withIdentifier: "SelectCaptainVC") as! SelectCaptainVC
            selectCapVC.teamArray = totalPlayers
            selectCapVC.saveTeamDelegate = self
            navigationController?.pushViewController(selectCapVC, animated: true)
            
        } else  {
            self.alert(message: "Team is incomplete")
        }
    }
    
    //MARK: - Select Player Button VIP-
    @IBAction func selectPlayer(_ sender: UIButton) {
        let indexpath = IndexPath(row: sender.tag, section: 0)
        switch selectedCategory {
        case .bat:
            checkOrUncheckBox(indexPath: indexpath, sender: &players.batsman[sender.tag])
        case .ball:
            checkOrUncheckBox(indexPath: indexpath, sender: &players.bowler[sender.tag])
        case .batBall:
            checkOrUncheckBox(indexPath: indexpath, sender: &players.allrounder[sender.tag])
        case .glove:
            checkOrUncheckBox(indexPath: indexpath, sender: &players.wicketKeeper[sender.tag])
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}


//MARK: - User Define Function EX -

extension SelectTeamVC {
    
    //MARK: - Hndel Refresh -
    @objc func handleRefresh(_: Any) {
        getSelectedPlayersApi()
    }
    
    // make checkbox fil and unfill
    func checkOrUncheckBox(indexPath: IndexPath, sender: inout SportMan) {
        if sender.isSelected {
            removeFromArray(indexPath: indexPath, sender: &sender)
        } else {
            AddIntoArray(indexPath: indexPath, sender: &sender)
        }
    }
    //MARK: - Remove Player Into Lists -
    func removeFromArray(indexPath: IndexPath,sender: inout SportMan) {
        switch selectedCategory {
        case .bat:
            removeBatsman(indexPath: indexPath, sender: &sender)
        case .ball:
            removeBowler(indexPath: indexPath, sender: &sender)
        case .batBall:
            removeAllRounder(indexPath: indexPath, sender: &sender)
        case .glove:
            removeKeeper(indexPath: indexPath, sender: &sender)
        }
    }
    func removeBatsman(indexPath: IndexPath, sender: inout SportMan)  {
        if s_batsman.count >= 0 {
            if let index = s_batsman.index(where: { $0.player_id == sender.player_id }) {
                s_batsman.remove(at: index)
                batsManLbl.text = String(describing: s_batsman.count)
                unfillCheckBox(indexPath: indexPath)
                sender.isSelected = false
                plusBudget(sender: sender)
                //                if isHaveATeam! { minusTransfer() }
            }
        }
    }
    func removeBowler(indexPath: IndexPath, sender: inout SportMan)  {
        if s_bowler.count >= 0 {
            if let index = s_bowler.index(where: { $0.player_id == sender.player_id }) {
                s_bowler.remove(at: index)
                ballerLbl.text = String(describing: s_bowler.count)
                unfillCheckBox(indexPath: indexPath)
                sender.isSelected = false
                plusBudget(sender: sender)
                
            }
        }
    }
    func removeAllRounder(indexPath: IndexPath, sender: inout SportMan)  {
        if s_allrounder.count >= 0 {
            if let index = s_allrounder.index(where: { $0.player_id == sender.player_id }) {
                s_allrounder.remove(at: index)
                allRounderLbl.text = String(describing: s_allrounder.count)
                unfillCheckBox(indexPath: indexPath)
                sender.isSelected = false
                plusBudget(sender: sender)
                //                if isHaveATeam { minusTransfer() }
            }
        }
    }
    func removeKeeper(indexPath: IndexPath, sender: inout SportMan)  {
        if s_wicketKeeper.count >= 0 {
            if let index = s_wicketKeeper.index(where: { $0.player_id == sender.player_id }) {
                s_wicketKeeper.remove(at: index)
                gloveLbl.text = String(describing: s_wicketKeeper.count)
                unfillCheckBox(indexPath: indexPath)
                sender.isSelected = false
                plusBudget(sender: sender)
                
            }
        }
    }
    
    //MARK: - Add Player Into Lists -
    func AddIntoArray(indexPath: IndexPath, sender: inout SportMan) {
        switch selectedCategory {
        case .bat:
            addBatsMan(indexPath: indexPath, sender: &sender)
        case .ball:
            addbowler(indexPath: indexPath, sender: &sender)
        case .batBall:
            addAllRounder(indexPath: indexPath, sender: &sender)
        case .glove:
            addWickertKiper(indexPath: indexPath, sender: &sender)
        }
    }
    func addBatsMan(indexPath: IndexPath,sender: inout SportMan)  {
        if checkBudget() {
            if s_batsman.count < 3 && minusBudget(sender: sender) {
                s_batsman.append(sender)
                batsManLbl.text = String(describing: s_batsman.count)
                fillCheckBox(indexPath: indexPath)
                sender.isSelected = true
            }
        } else {
            // you have not enough budget
            print("your budget is zero")
            
        }
    }
    func addbowler(indexPath: IndexPath, sender: inout SportMan)  {
        if checkBudget() {
            if s_bowler.count < 3 && minusBudget(sender: sender) {
                s_bowler.append(sender)
                ballerLbl.text = String(describing: s_bowler.count)
                fillCheckBox(indexPath: indexPath)
                sender.isSelected = true
            }
        } else {
            // you have not enough budget
            print("your budget is zero")
            
        }
    }
    func addAllRounder(indexPath: IndexPath, sender: inout SportMan)  {
        if checkBudget() {
            if s_allrounder.count < 4 && minusBudget(sender: sender){
                
                s_allrounder.append(sender)
                allRounderLbl.text = String(describing: s_allrounder.count)
                fillCheckBox(indexPath: indexPath)
                sender.isSelected = true
            }
            
        } else {
            // you have not enough budget
            print("your budget is zero")
            
        }
    }
    func addWickertKiper(indexPath: IndexPath,sender: inout SportMan)  {
        if checkBudget() {
            if s_wicketKeeper.count < 1 && minusBudget(sender: sender) {
                s_wicketKeeper.append(sender)
                gloveLbl.text = String(describing: s_wicketKeeper.count)
                fillCheckBox(indexPath: indexPath)
                sender.isSelected = true
            }
        } else {
            // you have not enough budget
            print("your budget is zero")
        }
    }
    
    //MARK: - Set Budget and Transfer -
    func setBudgetTransfer(){
        transfersLabel.text = players.user_transfers
        budgetLabel.text = players.user_budget
        budget = Int(players.user_budget)!
        transfer = Int(players.user_transfers)!
    }
    func minusBudget(sender: SportMan) -> Bool {
        let playerBudget = Int(sender.budget)
        let checkBudget = budget - playerBudget!
        if checkBudget >= 0 {
            budget = checkBudget
            budgetLabel.text = String(describing: budget)
            return true
        } else {
            //show alert message that budget is low
            print("Low Budget")
            budgetLabel.shake()
            return false
        }
    }
    func plusBudget(sender: SportMan)  {
        let playerBudget = Int(sender.budget)
        budget = budget + playerBudget!
        budgetLabel.text = String(describing: budget)
    }
    func checkBudget () -> Bool {
        if budget > 0 {
            return true
        } else {
            return false
        }
    }
    //MARK: - Fill and Unfill the check Box -
    func fillCheckBox(indexPath: IndexPath) {
        let currentCell = tableView.cellForRow(at: indexPath) as! SelectPlayerTVC
        currentCell.chackBox.image = UIImage(named: "checkbox")
    }
    
    func unfillCheckBox(indexPath: IndexPath) {
        let currentCell = tableView.cellForRow(at: indexPath) as! SelectPlayerTVC
        currentCell.chackBox.image = UIImage(named: "checkBoxEmpty")
    }
    
    
    //MARK: - Change the color of view under category -
    func changeColorOfSelectdView(selectedView: SelectedCategory) {
        switch selectedView {
        case .bat:
            changeColor(selectedView: batView)
            headerText = "Batsman"
            updateTableView(selecedView: batView)
        case .ball:
            changeColor(selectedView: ballView)
            headerText = "Bowler"
            updateTableView(selecedView: ballView)
        case .batBall:
            changeColor(selectedView: batBallView)
            headerText = "All Rounder"
            updateTableView(selecedView: batBallView)
        case .glove:
            changeColor(selectedView: glovesView)
            headerText = "Wicket Keeper"
            updateTableView(selecedView: glovesView)
        }
    }
    
    func changeColor(selectedView: UIView) {
        //first make all view white and then change the selectd view color green
        UIView.animate(withDuration: 0.3, animations: {
            self.batView.backgroundColor = UIColor.white
            self.ballView.backgroundColor = UIColor.white
            self.batBallView.backgroundColor = UIColor.white
            self.glovesView.backgroundColor = UIColor.white
            selectedView.backgroundColor = UIColor(red: 1.0/255.0, green: 149.0/255.0, blue: 37.0/255.0, alpha: 1.0)
        }, completion: nil)
        
    }
    
    //MARK: - Table view Helping Method -
    func updateTableView(selecedView: UIView) {
        if selecedView.backgroundColor != UIColor.white {
            self.tableView.reloadData()
        }
    }
    // This method is for number of rows in tableveiw
    func countTotalNumberOFPersonInArray () ->  Int {
        switch selectedCategory {
        case .bat:
            return players.batsman.count
        case .ball:
            return players.bowler.count
        case .batBall:
            return players.allrounder.count
        case .glove:
            return players.wicketKeeper.count
        }
    }
    
    func getPlayerIdAndCategory() -> (player_id: [String], category_id: [String]) {
        // This function combine all array of player and then retive player ids and categoryis form array and return
        // the two seprate arrays of category and id
        let totalPlayer =  s_batsman + s_bowler + s_wicketKeeper + s_allrounder
        var player_id = [String]()
        var player_category = [String]()
        
        for player in totalPlayer {
            player_id.append(player.player_id)
            player_category.append(player.category_id)
        }
        return (player_id, player_category)
    }
    
    //MARK: - Fill the Player Array if Selected -
    func populateAllPlayersArray(tempPlayers: Players){
        
        for batmans in tempPlayers.batsman {
            if batmans.isSelected {
                s_batsman.append(batmans)
            }
        }
        for bowler in tempPlayers.bowler {
            if bowler.isSelected {
                s_bowler.append(bowler)
            }
        }
        for allrounder in tempPlayers.allrounder {
            if allrounder.isSelected {
                s_allrounder.append(allrounder)
            }
        }
        for wicketKeeper in tempPlayers.wicketKeeper {
            if wicketKeeper.isSelected {
                s_wicketKeeper.append(wicketKeeper)
            }
        }
        updateLabel()
    }
    func updateLabel() {
        batsManLbl.text = String(describing: s_batsman.count)
        ballerLbl.text = String(describing: s_bowler.count)
        allRounderLbl.text = String(describing: s_allrounder.count)
        gloveLbl.text = String(describing: s_wicketKeeper.count)
    }
}

//MARK: - TableView Extension -

extension SelectTeamVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerview = Bundle.main.loadNibNamed("PlayerCategoryHeader", owner: self, options: nil)?.first as! PlayerCategoryHeader
        headerview.categoryLabel.text = headerText
        return headerview
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countTotalNumberOFPersonInArray()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch selectedCategory {
        case .bat:
            let cell = tableView.dequeueReusableCell(withIdentifier: Constant().CELL_SelectPlayer, for: indexPath) as! SelectPlayerTVC
            cell.setData(index: indexPath.row, man: players.batsman[indexPath.row])
            return cell
        case .ball:
            let cell = tableView.dequeueReusableCell(withIdentifier: Constant().CELL_SelectPlayer, for: indexPath) as! SelectPlayerTVC
            cell.setData(index: indexPath.row, man: players.bowler[indexPath.row])
            return cell
        case .batBall:
            let cell = tableView.dequeueReusableCell(withIdentifier: Constant().CELL_SelectPlayer, for: indexPath) as! SelectPlayerTVC
            cell.setData(index: indexPath.row, man: players.allrounder[indexPath.row])
            return cell
        case .glove:
            let cell = tableView.dequeueReusableCell(withIdentifier: Constant().CELL_SelectPlayer, for: indexPath) as! SelectPlayerTVC
            cell.setData(index: indexPath.row, man: players.wicketKeeper[indexPath.row])
            return cell
        }
        
    }
    
}

// MARK: - API extension -
extension SelectTeamVC {
    // use this api when user is login first time
    func callGetAllPlayers() {
        appDelegate.showActivityIndicatory(uiView: self.view)
        let parameters = ["user_id": UserDefaults.standard.string(forKey: Constant().UD_userId)!] as [String : Any]
        
        WebServices.getAllPlayers(parameters: parameters, success: { (players) in
            
            self.appDelegate.hideActivityIndicatory()
            self.players = players
            self.setBudgetTransfer()
            self.tableView.reloadData()
            
        }, apifailuer: { (error) in
            
            self.appDelegate.hideActivityIndicatory()
            self.alert(message: error)
            
        }) { (error) in
            
            self.appDelegate.hideActivityIndicatory()
            self.alert(message: error)
        }
    }
    
    // Use this API when Edit button is pressed
    func getSelectedPlayersApi() {
        refreshControl.endRefreshing()
        appDelegate.showActivityIndicatory(uiView: self.view)
        let parameters = ["user_id": UserDefaults.standard.string(forKey: Constant().UD_userId)!] as [String : Any]
        
        WebServices.getSelectedPlayers(parameters: parameters, success: { (players) in
            
            self.appDelegate.hideActivityIndicatory()
            self.players = players
            //populate the array
            self.populateAllPlayersArray(tempPlayers: players)
            self.setBudgetTransfer()
            self.tableView.reloadData()
            
        }, apifailuer: { (error) in
            
            self.appDelegate.hideActivityIndicatory()
            self.alert(message: error)
            
        }) { (error) in
            
            self.appDelegate.hideActivityIndicatory()
            self.alert(message: error)
        }
    }
}

extension SelectTeamVC: SaveTeam {
    // this is a protocol delegation which is tirgger after captain and vice caption is selectd.
    func hideCurentView() {
        self.dismiss(animated: true, completion: nil)
    }
    func sendDateToServer(captainId: String, viceCaptainId: String) {
        
        appDelegate.showActivityIndicatory(uiView: self.view)
        let team = getPlayerIdAndCategory()
        let parameters: [String: Any] = ["user_id": UserDefaults.standard.string(forKey: Constant().UD_userId)!,
                                         "budget": budgetLabel.text!,
                                         "category_id": team.category_id,
                                         "player_id": team.player_id,
                                         "captain": captainId,
                                         "vice_captain": viceCaptainId]
        
        if isHaveATeam {
            //MARK: / Update Team /
            WebServices.updateTeam(parameters: parameters, success: { (status) in
                self.appDelegate.hideActivityIndicatory()
                if status == "True" {
                    self.navigationController?.popViewController(animated: true)
                } else {
                    self.alert(message: "Request Failer")
                }
                
            }, apifailuer: { (error) in
                
                self.appDelegate.hideActivityIndicatory()
                self.alert(message: error)
            }) { (error) in
                
                self.appDelegate.hideActivityIndicatory()
                self.alert(message: error)
            }
        } else {
            //MARK: / Save Team First Time /
            WebServices.saveSelectedTeam(parameters: parameters, success: { (status) in
                self.appDelegate.hideActivityIndicatory()
                if status == "True" {
                    self.dismiss(animated: true, completion: nil)
                } else {
                    self.alert(message: "Request Failer")
                }
                
            }, apifailuer: { (error) in
                
                self.appDelegate.hideActivityIndicatory()
                self.alert(message: error)
            }) { (error) in
                
                self.appDelegate.hideActivityIndicatory()
                self.alert(message: error)
            }
        }
    }
}







