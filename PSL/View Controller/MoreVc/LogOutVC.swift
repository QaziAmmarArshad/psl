//
//  LogOutVC.swift
//  PSL
//
//  Created by Mudassar on 18/01/2018.
//  Copyright © 2018 HalfTech. All rights reserved.
//

import UIKit
import GoogleSignIn

class LogOutVC: UIViewController {
    
    var sections = ["", "About", "Others"]
    var rows = [["LogOut", "Setting"],["About Us", "Help & Rules"],["Partners","F&C","Privacy Ploicy", "FAQs","Contact Us"]]
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.prefersLargeTitles = true
        // Do any additional setup after loading the view.
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func logOut(_ sender: UIButton) {
        let tag = sender.tag
        switch tag {
        case 0:
            let defauts  = UserDefaults.standard
            defauts.removeObject(forKey: Constant().UD_Email)
            defauts.removeObject(forKey: Constant().UD_Password)
            GIDSignIn.sharedInstance().signOut()
        default:
            print("this is the tag that is selected \(tag)")
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

//MARK: - TableView Extension -
extension LogOutVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    //
    //    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    //
    //        let headerView = Bundle.main.loadNibNamed("MoreTVCHeader", owner: self, options: nil)?.first as! MoreTVCHeader
    //        headerView.headerLabel.text = sections[section]
    //        if section == 0 {
    //            headerView.headerLabel.font = headerView.headerLabel.font.withSize(40)
    //        }
    //        return headerView
    //    }
    //
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rows[section].count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let headerView = Bundle.main.loadNibNamed("MoreTVCHeader", owner: self, options: nil)?.first as! MoreTVCHeader
        if indexPath.row == 0 {
            // load header
            headerView.headerLabel.text = sections[indexPath.section]
            return headerView
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MoreCellTVC", for: indexPath) as! MoreCellTVC
            if  indexPath.row == 1 && indexPath.section == 0 {
                //set log out View
                cell.setLogOutCell()
                return cell
            }
            cell.labelText.text = rows[indexPath.section][indexPath.row - 1]
            cell.actionButton.tag = reutrnTag(indexPath: indexPath)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 && indexPath.section == 0 {
            return 0
        } else {
            return UITableViewAutomaticDimension
        }
    }
    
    func reutrnTag(indexPath: IndexPath) -> Int {
        var tag = 0
        if indexPath.section == 0 {
            tag = indexPath.row
        } else if indexPath.section == 1{
            tag = rows[indexPath.section - 1].count + indexPath.row
        } else {
            tag = rows[indexPath.section - 1].count + rows[indexPath.section - 2].count + indexPath.row
        }
        return tag
    }
}




