//
//  MoreCellTVC.swift
//  PSL
//
//  Created by Mudassar on 29/01/2018.
//  Copyright © 2018 HalfTech. All rights reserved.
//

import UIKit

class MoreCellTVC: UITableViewCell {

    @IBOutlet weak var labelText: UILabel!
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var greenView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setLogOutCell() {
        
        actionButton.tag = 0
        iconImage.isHidden = true
        greenView.alpha = 0.5
        labelText.text = "LogOut"
        labelText.textAlignment = .center
    }
    
    func setData() {
    }
    
}
