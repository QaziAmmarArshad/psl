//
//  FixturesViewController.swift
//  PSL
//
//  Created by Mudassar on 15/01/2018.
//  Copyright © 2018 HalfTech. All rights reserved.
//

import UIKit

class FixturesViewController: UIViewController {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var fixturesAndResults =  FixturesAndResults()
    var refreshControl = UIRefreshControl()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self as? UIGestureRecognizerDelegate
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        // Do any additional setup after loading the view.
        callHomeApi()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    //MARK: - Pull TO Refres -
    
    func setPullToRefresh() {
//        refreshControl.attributedTitle = NSAttributedString(string: " ")
//        refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: UIControlEvents.valueChanged)
//        self.tableView.addSubview(refreshControl) // not required when using UITableViewController
        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.addTarget(self, action: #selector(handelRefresh(_:)), for: UIControlEvents.valueChanged)
    }
    
    @objc func handelRefresh(_: Any) {
        callHomeApi()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
// MARK: - Table view Extensions -
extension FixturesViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if fixturesAndResults.statuss == "True" {
            return fixturesAndResults.fixturesResults.count } else { return 0 }
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let date = DateController.setDate(string_date: fixturesAndResults.fixturesResults[section].matchdate)
        return date
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fixturesAndResults.fixturesResults[section].results.count + fixturesAndResults.fixturesResults[section].matches.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = Bundle.main.loadNibNamed("MatchBetweenTableViewCell", owner: self, options: nil)?.first as! MatchBetweenTableViewCell
        let counter = fixturesAndResults.fixturesResults[indexPath.section].matches.count
        if indexPath.row < counter {
            cell.setCellData(match: fixturesAndResults.fixturesResults[indexPath.section].matches[indexPath.row])
        } else {
            cell.setCellMatchResult(match: fixturesAndResults.fixturesResults[indexPath.section].results[indexPath.row - counter])
        }
        return cell
    }
}
// MARK: - Api Cal -
extension FixturesViewController {
    func callHomeApi(){
        appDelegate.showActivityIndicatory(uiView: self.view)
         let parameter: [String: String] = ["user_id": UserDefaults.standard.string(forKey: Constant().UD_userId)!]
        
        WebServices.getFixturesAndResults(parameters: parameter, success: { (fixturesAndResultsObj) in
            self.fixturesAndResults = fixturesAndResultsObj
            self.tableView.reloadData()
            self.appDelegate.hideActivityIndicatory()
        }, apifailuer: { (error) in
            self.appDelegate.hideActivityIndicatory()
            self.alert(message: error)
        }) { (error) in
            self.appDelegate.hideActivityIndicatory()
            self.alert(message: error)
        }
    }
}





