//
//  SelectTeamViewController.swift
//  PSL
//
//  Created by Mudassar on 05/01/2018.
//  Copyright © 2018 HalfTech. All rights reserved.
//

import UIKit

class SelectTeamViewController: UIViewController {
    
    // MARK - IBOutlets -
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var pickerHeaderView: UIView!
    @IBOutlet weak var selectedTeamLabel: UILabel!
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var teamArray = [TeamDetail]()
    var selectedTeam = ""
    var selecedTeamId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        hidePickerView()
        callGetAllTeam()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK - Button Action -
    @IBAction func openPicker(_ sender: Any) {
        if pickerView.isHidden {
            animateView(ishiddedn: false)
        }
    }
    @IBAction func selectPickerItem(_ sender: Any) {
        if !selectedTeam.isEmpty {
            selectedTeamLabel.text = selectedTeam
            animateView(ishiddedn: true )
        }
    }
    
    @IBAction func moveTonextScreen(_ sender: Any) {
        if selectedTeam.isEmpty {
            self.alert(message: "Please Select Team")
        } else {
           //move to next screen
            let TeamNameVC = storyboard?.instantiateViewController(withIdentifier: "TeamNameViewController") as! TeamNameViewController
            //setting value to next screen
            TeamNameVC.selectedTeamId = self.selecedTeamId
            navigationController?.pushViewController(TeamNameVC, animated: true)
        }
    }
    @IBAction func backButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    
    //show picker view with animation
    private func animateView(ishiddedn: Bool) {
        //this if show the view with no animation and when we hide the view is show the animation
        var withDuration = 0.0
        if ishiddedn {
            withDuration = 0.5
        }
        UIView.animate(withDuration: withDuration, animations: {
            self.pickerView.isHidden = ishiddedn
            self.pickerHeaderView.isHidden = ishiddedn
        }, completion: nil)
    }
    //At Start hide picker view and button on its top
    private func hidePickerView() {
        self.pickerView.isHidden = true
        self.pickerHeaderView.isHidden = true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - Picker View Extension -
extension SelectTeamViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return teamArray.count + 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if row == 0 {
            return "- Select Your Favorite Team -"
        }
        return teamArray[row - 1].name
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if row == 0 {
            selectedTeam = ""
        } else {
            selectedTeam = teamArray[row - 1].name
            selecedTeamId = teamArray[row - 1].team_id
        }
    }
}

//MARK: Calling Api Extension
extension SelectTeamViewController {
    func callGetAllTeam() {
        WebServices.getAllTeams(success: { (parseTeamArray) in
            self.appDelegate.hideActivityIndicatory()
            if parseTeamArray.statuss == "True" {
                self.teamArray = parseTeamArray.teams
                self.pickerView.reloadAllComponents()
            } else {
                self.alert(message: parseTeamArray.message)
            }
        }, apifailuer: { (error) in
            self.appDelegate.hideActivityIndicatory()
            self.alert(message: error)
        }) { (error) in
            self.appDelegate.hideActivityIndicatory()
            self.alert(message: error)
        }
    }
}















