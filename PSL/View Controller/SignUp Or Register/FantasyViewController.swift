//
//  FantasyViewController.swift
//  PSL
//
//  Created by Mudassar on 03/01/2018.
//  Copyright © 2018 HalfTech. All rights reserved.
//

import UIKit

class FantasyViewController: UIViewController {

    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        checkUserIsLogin()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    
    func checkUserIsLogin() {
        let email = UserDefaults.standard.string(forKey: Constant().UD_Email) ?? ""
        let password =  UserDefaults.standard.string(forKey: Constant().UD_Password) ?? ""
        if email.isEmpty {
            // do nothing
        } else {
            print("user Defaults are not nil so move to logged In screen")
            login(password: password, email: email)
        }
    }
    
    func movetoNextScreen(user: User) {
        if user.user_team.isEmpty || user.user_favourite_team == "0" {
            // then move to select team pages otherwise move to fantasy leuge
            let SelectTeamVC = storyboard?.instantiateViewController(withIdentifier: "SelectTeamViewController") as! SelectTeamViewController
            navigationController?.pushViewController(SelectTeamVC, animated: true)
        } else {
            // if user sign information is completed then move to fantasy Logged In View controller
            let fantasyVC = storyboard?.instantiateViewController(withIdentifier: "FantesyLoggedInVC") as! FantesyLoggedInVC
            if !user.selected_team {
                fantasyVC.isHaveATeam = false
            } else {
                fantasyVC.isHaveATeam = true
            }
            if !user.create_league.isEmpty {
                fantasyVC.leugeName = user.create_league
            }
            navigationController?.pushViewController(fantasyVC, animated: false)
        }
    }
}

// MARK: - Login Api Extension
extension FantasyViewController {
    func login(password: String, email: String){
        
        appDelegate.showActivityIndicatory(uiView: self.view)
        let parametrs : [String : Any] = [
            "password": password,
            "email": email
        ]
        WebServices.loginApi(parametrs: parametrs, success: { (user) in
            self.appDelegate.hideActivityIndicatory()
            if user.statuss == "True" {
                //move to next screen
                self.movetoNextScreen(user: user)
            } else {
                self.alert(message: user.message)
            }
        }, apifailuer: { (error) in
            self.appDelegate.hideActivityIndicatory()
            self.alert(message: error)
        }) { (error) in
            self.appDelegate.hideActivityIndicatory()
            self.alert(message: error)
        }
    }
}
