//
//  SelectCaptainVC.swift
//  PSL
//
//  Created by Mudassar on 17/01/2018.
//  Copyright © 2018 HalfTech. All rights reserved.
//

import UIKit



enum Caption {
    case caption, viceCaption
    init() {
        self = .caption
    }
}

class SelectCaptainVC: UIViewController {
    
    @IBOutlet weak var headerLbl: UILabel!
    var teamArray = [SportMan]()
    var caption: SportMan?
    var vice_captain: SportMan?
    var captionCategory = Caption()
    var captionIndex = -1
    var saveTeamDelegate: SaveTeam?
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        //        for player in teamArray {
        //            player.isSelected = false
        //        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func conformButton(_ sender: Any) {
        switch captionCategory {
        case .caption:
            showConformAlert(message: "You want to Conform this Captian")
        case .viceCaption:
            showConformAlert(message: "You are sure to conform this as vice_Caption")
        }
        showConformAlert(message: "")
    }
    
    @IBAction func backButton(_ sender: UIButton) {
//        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func playerTouched(_ sender: UIButton) {
        let index = sender.tag
        switch captionCategory {
        //chosig caption alghorithum
        case .caption:
            if isCaptionIsEmpty() || !teamArray[index].isSelected {
                if teamArray[index].isSelected {
                    fillCheckBox(indexPath: IndexPath(row: index, section: 0))
                    teamArray[index].isSelected = false
                    caption = teamArray[index]
                    captionIndex = index
                } else {
                    unfillCheckBox(indexPath: IndexPath(row: index, section: 0))
                    teamArray[index].isSelected = true
                    caption = nil
                }
            } else {
                self.alert(message: "Only One Captain can be selected")
            }
        case .viceCaption:
            //if vice caption is selected
            if captionIndex == index {
                self.alert(message: "This is Caption Chose Other")
            } else {
                if isViceCaptainIsEmpty() || !teamArray[index].isSelected {
                    if teamArray[index].isSelected {
                        fillCheckBox(indexPath: IndexPath(row: index, section: 0))
                        teamArray[index].isSelected = false
                        vice_captain = teamArray[index]
                    } else {
                        unfillCheckBox(indexPath: IndexPath(row: index, section: 0))
                        teamArray[index].isSelected = true
                        vice_captain = nil
                    }
                } else {
                    self.alert(message: "Only One Vice Captain can be selected")
                }
            }
        }
    }
    
    
    func fillCheckBox(indexPath: IndexPath) {
        let currentCell = tableView.cellForRow(at: indexPath) as! SelectCaptainTVC
        currentCell.chackBox.image = UIImage(named: "checkbox")
    }
    
    func unfillCheckBox(indexPath: IndexPath) {
        let currentCell = tableView.cellForRow(at: indexPath) as! SelectCaptainTVC
        currentCell.chackBox.image = UIImage(named: "checkBoxEmpty")
    }
    
    func isCaptionIsEmpty() -> Bool {
        if caption == nil {
            return true
        } else {
            return false
        }
    }
    
    func isViceCaptainIsEmpty() -> Bool {
        if vice_captain == nil {
            return true
        } else {
            return false
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass th1e selected object to the new view controller.
     }
     */
    
    func showConformAlert(message: String) {
        let alert = UIAlertController(title: "PSL", message: message, preferredStyle: .alert)
        let okAction  = UIAlertAction(title: "Conform", style: .default) { (UIAlertAction) in
            switch self.captionCategory {
            case .caption:
                // when caption is selected
                self.captionCategory = .viceCaption
                self.headerLbl.text = "Vice Captain"
                self.alert(message: "Select vice_caption")
            case .viceCaption:
                //pop view controllr and save the
                 self.saveTeamDelegate?.sendDateToServer(captainId: (self.caption?.player_id)!, viceCaptainId: (self.vice_captain?.player_id)!)
                self.navigationController?.popViewController(animated: true)
               
            }
            
        }
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
}

extension SelectCaptainVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return teamArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectCaptainTVC", for: indexPath) as! SelectCaptainTVC
        cell.setData(index: indexPath.row, man: teamArray[indexPath.row])
        return cell
    }
    
}
