//
//  SignUpViewController.swift
//  PSL
//
//  Created by Mudassar on 05/01/2018.
//  Copyright © 2018 HalfTech. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {
    
    // MARK - IBOutlet
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var pickerHeaderView: UIView!
    @IBOutlet weak var nameTextFiled: UITextField!
    @IBOutlet weak var emailAddressText: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var countryName: UITextField!
    
     let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var countyArray = [String]()
    var selectedCountry = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        hidePickerView()
        countyArray = getAllCountriesList()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: - Button Actions -
    @IBAction func openPicker(_ sender: Any) {
        if pickerView.isHidden {
            animateView(ishiddedn: false)
        }
    }
    @IBAction func selectPickerItem(_ sender: Any) {
        if !selectedCountry.isEmpty {
            countryName.text = selectedCountry
            animateView(ishiddedn: true )
        }
    }
    
    @IBAction func moveTonextScreen(_ sender: Any) {
        if IsAnyTextFieldEmpty() {
            self.alert(message: "Please Fill All Fiels")
        } else {
            //move to next screen
            signUpApi()
        }
    }
    @IBAction func backButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

// MARK: - User Define Function Extension
extension SignUpViewController {
    
    //At Start hide picker view and button on its top
    private func hidePickerView() {
        self.pickerView.isHidden = true
        self.pickerHeaderView.isHidden = true
    }
    //generate All contries list
    private func getAllCountriesList() -> [String] {
        
        var countries: [String] = []
        for code in NSLocale.isoCountryCodes as [String] {
            let id = NSLocale.localeIdentifier(fromComponents: [NSLocale.Key.countryCode.rawValue: code])
            let name = NSLocale(localeIdentifier: "en_UK").displayName(forKey: NSLocale.Key.identifier, value: id) ?? "Country not found for code: \(code)"
            countries.append(name)
        }
        return countries
    }
    
    //show picker view with animation
    private func animateView(ishiddedn: Bool) {
        //this if show the view with no animation and when we hide the view is show the animation
        var withDuration = 0.0
        if ishiddedn {
            withDuration = 0.5
        }
        UIView.animate(withDuration: withDuration, animations: {
            self.pickerView.isHidden = ishiddedn
            self.pickerHeaderView.isHidden = ishiddedn
        }, completion: nil)
    }
    //check that all text fields are filled or not
    func IsAnyTextFieldEmpty() -> Bool {
                if (nameTextFiled.text?.isEmpty)! || (emailAddressText.text?.isEmpty)! || (passwordTextField.text?.isEmpty)! || (phoneNumberTextField.text?.isEmpty)! || (countryName.text?.isEmpty)! {
                    return true
                } else {
                    return false
        }
    }
    
    func saveUserDefault(user: User){
        UserDefaults.standard.set(user.user_id, forKey: "user_id")
        UserDefaults.standard.set(emailAddressText.text!, forKey: "email")
        UserDefaults.standard.set(user.token, forKey: "token")
        UserDefaults.standard.set(nameTextFiled.text, forKey: "name")
        UserDefaults.standard.set(passwordTextField.text!, forKey: "password")
        WebServices.updateHeader()
        movetoNextScreen()
    }
    
    func movetoNextScreen() {
        let SelectTeamVC = storyboard?.instantiateViewController(withIdentifier: "SelectTeamViewController") as! SelectTeamViewController
        navigationController?.pushViewController(SelectTeamVC, animated: true)
    }
}

// MARK: - pickerview delegate and datasource-
extension SignUpViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countyArray.count + 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if row == 0 {
            return "- Select Country -"
        }
        return countyArray[row - 1]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if row == 0 {
            selectedCountry = ""
        } else {
            selectedCountry = countyArray[row - 1]
        }
    }
}

//MARK: -Api Calling Extension -
extension SignUpViewController {
    func signUpApi(){
        //webservices
         appDelegate.showActivityIndicatory(uiView: self.view)
        let parametrs : [String : Any] = ["username" : nameTextFiled.text! ,
                                          "password": passwordTextField.text!,
                                          "country": countryName.text!,
                                          "phone": phoneNumberTextField.text!,
                                          "email": emailAddressText.text!
                                          ]
        WebServices.SignApi(parametrs: parametrs, success: { (user) in
            self.appDelegate.hideActivityIndicatory()
            if user.statuss == "True" {
                self.saveUserDefault(user: user)
            } else {
                self.alert(message: user.message)
            }
        }, apifailuer: { (error) in
            self.appDelegate.hideActivityIndicatory()
            self.alert(message: error)
        }) { (error) in
            self.appDelegate.hideActivityIndicatory()
            self.alert(message: error)
        }
    }
}




