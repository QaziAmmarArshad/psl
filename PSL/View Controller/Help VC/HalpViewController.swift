//
//  HalpViewController.swift
//  PSL
//
//  Created by Mudassar on 15/01/2018.
//  Copyright © 2018 HalfTech. All rights reserved.
//

import UIKit

class HalpViewController: UIViewController {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var help = Help()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self as? UIGestureRecognizerDelegate
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        // Do any additional setup after loading the view.
        callHelpPageApi()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func backButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    func callHelpPageApi() {
        self.appDelegate.showActivityIndicatory(uiView: self.view)
        WebServices.getHelpPage(success: { (parseHelp) in
            self.appDelegate.hideActivityIndicatory()
            self.help = parseHelp
            self.tableView.reloadData()
            
        }, apifailuer: { (error) in
            
            self.appDelegate.hideActivityIndicatory()
            self.alert(message: error)
        }) { (error) in
            
            self.appDelegate.hideActivityIndicatory()
            self.alert(message: error)
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

//MARK: - TableView Extension -
extension HalpViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return help.rules.count + 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HelpPointTVC", for: indexPath) as! HelpPointTVC
       
        switch indexPath.row {
        case 0:
            cell.pointLbl.isHidden = true
            cell.descriptionLbl.text = help.help
        default:
            cell.setCellData(rule: help.rules[indexPath.row - 1])
        }
        return cell
    }
}





