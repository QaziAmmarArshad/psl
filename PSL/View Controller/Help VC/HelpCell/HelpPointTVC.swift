//
//  HelpPointTVC.swift
//  PSL
//
//  Created by Mudassar on 16/01/2018.
//  Copyright © 2018 HalfTech. All rights reserved.
//

import UIKit

class HelpPointTVC: UITableViewCell {

    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var pointLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCellData(rule: Rules) {
        descriptionLbl.text = rule.rule
        pointLbl.text = rule.point
    }

}
