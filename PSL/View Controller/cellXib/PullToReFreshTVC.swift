//
//  PullToReFreshTVC.swift
//  PSL
//
//  Created by Mudassar on 26/01/2018.
//  Copyright © 2018 HalfTech. All rights reserved.
//

import UIKit

class PullToReFreshTVC: UITableViewCell {

    @IBOutlet weak var stackView: UIStackView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        stackView.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func showView() {
        stackView.isHidden = false
    }
}




