//
//  MatchBetweenTableViewCell.swift
//  PSL
//
//  Created by Mudassar on 02/01/2018.
//  Copyright © 2018 HalfTech. All rights reserved.
//

import UIKit

class MatchBetweenTableViewCell: UITableViewCell {

    //MARK: - IBOutles -
    @IBOutlet weak var winTeamName: UILabel!
    @IBOutlet weak var winTeamImage: UIImageView!
    @IBOutlet weak var socre: UILabel!
    @IBOutlet weak var loseTeamImage: UIImageView!
    @IBOutlet weak var loseTeamName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCellData(match: Matches) {
    
        winTeamName.text = match.team1Detail.nick_name
        socre.text =  match.time
        loseTeamName.text = match.team2Detail.nick_name
        
        winTeamImage.sd_setImage(with: URL(string: match.team1Detail.logo), placeholderImage: UIImage(named: Constant().PLACE_HOLDER_Image ))
        loseTeamImage.sd_setImage(with: URL(string: match.team2Detail.logo), placeholderImage: UIImage(named: Constant().PLACE_HOLDER_Image) )
    }
    
    func setCellMatchResult(match: MatchResults) {
        winTeamName.text = match.won_team_detail.nick_name
        socre.text =  match.won_team_score + "-" + match.loose_team_score
        loseTeamName.text = match.loose_team_detail.nick_name
        winTeamImage.sd_setImage(with: URL(string: match.won_team_detail.logo), placeholderImage: UIImage(named: Constant().PLACE_HOLDER_Image ))
        loseTeamImage.sd_setImage(with: URL(string: match.loose_team_detail.logo), placeholderImage: UIImage(named: Constant().PLACE_HOLDER_Image) )
    }
    
}






