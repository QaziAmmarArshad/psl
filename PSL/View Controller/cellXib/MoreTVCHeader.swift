//
//  MoreTVCHeader.swift
//  PSL
//
//  Created by Mudassar on 29/01/2018.
//  Copyright © 2018 HalfTech. All rights reserved.
//

import UIKit

class MoreTVCHeader: UITableViewCell {
    
    @IBOutlet weak var headerLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
