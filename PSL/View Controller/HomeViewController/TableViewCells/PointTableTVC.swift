//
//  PointTableTVC.swift
//  PSL
//
//  Created by Mudassar on 09/01/2018.
//  Copyright © 2018 HalfTech. All rights reserved.
//

import UIKit
import SwiftyJSON
import SDWebImage

class PointTableTVC: UITableViewCell {

    @IBOutlet weak var pos: UILabel!
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var W: UILabel!
    @IBOutlet weak var P: UILabel!
    @IBOutlet weak var L: UILabel!
    @IBOutlet weak var nrr: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(index: Int, pointTable: Points_Table){
        pos.text = String(describing: index + 1)
        name.text = pointTable.name
        W.text = pointTable.W
        P.text = pointTable.P
        L.text = pointTable.L
        nrr.text = pointTable.nrr
        logo.sd_setImage(with: URL(string: pointTable.logo), placeholderImage: UIImage(named: Constant().PLACE_HOLDER_Image))
    }
}




