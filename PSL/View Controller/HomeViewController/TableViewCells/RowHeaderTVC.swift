//
//  RowHeaderTVC.swift
//  PSL
//
//  Created by Mudassar on 03/01/2018.
//  Copyright © 2018 HalfTech. All rights reserved.
//

import UIKit

class RowHeaderTVC: UITableViewCell {

    @IBOutlet weak var heading: UIStackView!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var viewAllButton: GreenButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        hideAllView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setDate(string_date: String) {
        // setting date formate to Swift formate
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-mm-dd" //Your date format
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00") //Current time zone
        let date = dateFormatter.date(from: string_date) //according to date format your date string
        dateFormatter.dateStyle = .long
        let newDate = dateFormatter.string(from: date!) //pass Date here

        self.date.text = newDate
    }
    //hiding all view and show the view that you required in your row
    func hideAllView() {
        heading.isHidden = true
        date.isHidden = true
        viewAllButton.isHidden = true
    }
    func showFirst2Views() {
        heading.isHidden = false
        date.isHidden = false
        viewAllButton.isHidden = true
    }
    func showOnlyHeading() {
        heading.isHidden = false
        date.isHidden = true
        viewAllButton.isHidden = true
    }
    func showButton() {
        heading.isHidden = true
        date.isHidden = true
        viewAllButton.isHidden = false
    }
    
}







