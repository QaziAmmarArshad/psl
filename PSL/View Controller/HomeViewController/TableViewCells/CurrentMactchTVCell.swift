//
//  CurrentMactchTVCell.swift
//  PSL
//
//  Created by Mudassar on 08/01/2018.
//  Copyright © 2018 HalfTech. All rights reserved.
//

import UIKit
import SDWebImage

class CurrentMactchTVCell: UITableViewCell {

    @IBOutlet weak var matchDate: UILabel!
    @IBOutlet weak var winTeamName: UILabel!
    @IBOutlet weak var winTeamImage: UIImageView!
    @IBOutlet weak var socre: UILabel!
    @IBOutlet weak var loseTeamImage: UIImageView!
    @IBOutlet weak var loseTeamName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setTabelData(result: MatchResults) {
        matchDate.text = "Friday 24 Feburary 2018"
        winTeamName.text = result.won_team
        socre.text = result.won_team_score + "-" + result.loose_team_score
        loseTeamName.text = result.loose_team
        
        winTeamImage.sd_setImage(with: URL(string: result.won_team_detail.logo), placeholderImage: UIImage(named: Constant().PLACE_HOLDER_Image ))
        loseTeamImage.sd_setImage(with: URL(string: result.loose_team_detail.logo), placeholderImage: UIImage(named: Constant().PLACE_HOLDER_Image) )
    }
}





