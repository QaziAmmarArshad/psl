//
//  HomeViewController.swift
//  PSL
//
//  Created by Mudassar on 01/01/2018.
//  Copyright © 2018 HalfTech. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    //MARK: IBOutlets
    @IBOutlet weak var tableView: UITableView!
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var home = Home()
    var refreshControl: UIRefreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.callHomeApi()
        setPullToRefresh()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setPullToRefresh() {
//        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: " ")
        refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: UIControlEvents.valueChanged)
        self.tableView.addSubview(refreshControl) // not required when using UITableViewController
    }
    
    @objc func handleRefresh(_: Any) {
        callHomeApi()
    }
}

//MARK: TableView Extensioin
extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
   
    func numberOfSections(in tableView: UITableView) -> Int {
        if home.statuss == "True" {
            return 4
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countNumberOfRowInSection(section: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if home.statuss == "True" {
            if indexPath.section == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: Constant.CURRENTMATCH_CELL) as! CurrentMactchTVCell
                cell.setTabelData(result: home.results[0].match_results[0])
                return cell
            } else if indexPath.section == 1 {
                //MARK: - Fixture Cell -
                if indexPath.row == 0 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "UpcomingFixtures") as! RowHeaderTVC
                    cell.showFirst2Views()
                    cell.setDate(string_date: home.fixtures[0].matchdate)
                    return cell
                } else if indexPath.row == home.fixtures[0].matches.count + 1 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "UpcomingFixtures") as! RowHeaderTVC
                    cell.showButton()
                    return cell
                } else {
                    let cell = Bundle.main.loadNibNamed("MatchBetweenTableViewCell", owner: self, options: nil)?.first as! MatchBetweenTableViewCell
                    cell.setCellData(match: home.fixtures[0].matches[indexPath.row - 1])
                    return cell
                }
                
            } else if indexPath.section == 2 {
                //MARK:  - Point Table -
                if indexPath.row == 0 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "UpcomingFixtures") as! RowHeaderTVC
                    cell.showOnlyHeading()
                    cell.headingLabel.text = "League Table"
                    return cell
                } else if indexPath.row == 1 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: Constant.LEUGETOP_CELL )
                    return cell!
                } else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: Constant.LEUGELIST_CELL ) as! PointTableTVC
                    let index = indexPath.row - 2
                    cell.setData(index: index, pointTable: home.points_Tables[index])
                    return cell
                }
                
            } else {
                //MARK - Results Cell
                if indexPath.row == 0 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "UpcomingFixtures") as! RowHeaderTVC
                    cell.showFirst2Views()
                    cell.headingLabel.text = "Latest Results"
                    cell.setDate(string_date: home.results[0].matchdate)
                    return cell
                } else if indexPath.row == home.fixtures[0].matches.count + 1 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "UpcomingFixtures") as! RowHeaderTVC
                    cell.showButton()
                    return cell
                } else {
                    let cell = Bundle.main.loadNibNamed("MatchBetweenTableViewCell", owner: self, options: nil)?.first as! MatchBetweenTableViewCell
                    cell.setCellMatchResult(match: home.results[0].match_results[indexPath.row - 1])
                    return cell
                }
            }
        } else {
            // If Status if false then show the pull to refresh page
            // If any problem the load cell that show pull to refresh
            let cell = Bundle.main.loadNibNamed("PullToReFreshTVC", owner: self, options: nil)?.first as! PullToReFreshTVC
                cell.showView()
            
            return cell
        }
    }
}

//MARK: - All Function -
extension HomeViewController {
    //count number of rows in each section
    func countNumberOfRowInSection(section: Int) -> Int {
        if section == 0 {
            return 1
        } else if section == 1 {
            return 2 + home.fixtures[0].matches.count
        } else if section == 2 {
            return 2 + home.points_Tables.count
        } else {
            return 2 + home.results[0].match_results.count
        }
    }
}

//MARK: - Home APi calling Extension -
extension HomeViewController {
    func callHomeApi(){
        self.refreshControl.endRefreshing()
        appDelegate.showActivityIndicatory(uiView: self.view)
        WebServices.homeApi(success: { (homeObj) in
            self.home = homeObj
            
            self.tableView.reloadData()
            self.appDelegate.hideActivityIndicatory()
        }, apifailuer: { (error) in
           self.appDelegate.hideActivityIndicatory()
            self.alert(message: error)
        }) { (error) in
            self.appDelegate.hideActivityIndicatory()
            self.alert(message: error)
        }
    }
}




