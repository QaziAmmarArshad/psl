//
//  FaceBookGmailVC.swift
//  PSL
//
//  Created by Mudassar on 04/01/2018.
//  Copyright © 2018 HalfTech. All rights reserved.
//

import UIKit
import GoogleSignIn

class FaceBookGmailVC: UIViewController {
    
    //MARK: - IBOutlets -
    @IBOutlet weak var myActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    // MARK: - ViewControllerLifeCycle -
    override func loadView() {
        super.loadView()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        myActivityIndicator.isHidden = true
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
         FacebookLogin().delegate = self
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        email.text = ""
        passwordTF.text = ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Button Action -
    @IBAction func loginBtn(_ sender: Any) {
        if IsAnyTextFieldEmpty() {
            self.alert(message: "Please Fill All Fiels")
        } else {
            //move to next screen
            login()
        }
    }
    @IBAction func faceBookLogin(_ sender: Any) {
        let facebookObj = FacebookLogin()
        facebookObj.delegate = self
        facebookObj.login(viewController: self)
    }
    @IBAction func gmailLogin(_ sender: Any) {
        GIDSignIn.sharedInstance().signIn()
    }
    @IBAction func btnBackPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //check that all text fields are filled or not
    func IsAnyTextFieldEmpty() -> Bool {
        if (email.text?.isEmpty)! || (passwordTF.text?.isEmpty)! {
            return true
        } else {
            return false
        }
    }
    //MARK: - user defaults -
    func saveUserDefault(user: User) {
        
        UserDefaults.standard.set(user.user_id, forKey: "user_id")
        UserDefaults.standard.set(user.email, forKey: Constant().UD_Email)
        UserDefaults.standard.set(user.token, forKey: "token")
        UserDefaults.standard.set(user.name, forKey: "name")
        UserDefaults.standard.set(passwordTF.text!, forKey: Constant().UD_Password)
        WebServices.updateHeader()
        movetoNextScreen(user: user)
    }
    
    func movetoNextScreen(user: User) {
        if user.user_team.isEmpty || user.user_favourite_team == "0" {
            // then move to select team pages otherwise move to fantasy leuge
            let SelectTeamVC = storyboard?.instantiateViewController(withIdentifier: "SelectTeamViewController") as! SelectTeamViewController
            navigationController?.pushViewController(SelectTeamVC, animated: true)
        } else {
            // if user sign information is completed then move to fantasy View controller
            let fantasyVC = storyboard?.instantiateViewController(withIdentifier: "FantesyLoggedInVC") as! FantesyLoggedInVC
            if !user.selected_team {
                fantasyVC.isHaveATeam = false
            } else {
                fantasyVC.isHaveATeam = true
            }
            if !user.create_league.isEmpty {
                fantasyVC.leugeName = user.create_league
            }
            navigationController?.pushViewController(fantasyVC, animated: true)
        }
    }
}
//MARK: - FaceBook Login Extension -
extension FaceBookGmailVC: FacebookDelegate {
    
    func facebookSuccess(dictionaryValue: [String: Any]) {
        
        let responseDictionary = dictionaryValue
        let id = responseDictionary["id"] as? String
        let email = responseDictionary["email"] as? String
        let name = responseDictionary["name"] as? String
        let _ = responseDictionary["link"] as? String
        gmailSignUP(name: name!, userIdAs: id!, email: "f" + email!)
    }
    
    func facebookFailuer(error: String) {
        self.alert(message: error)
    }
    
    func setTextFiled(email: String, Password: String) {
        self.email.text = email
        passwordTF.text = Password
    }
}

//MARK: - Gmail Login Ex -
extension FaceBookGmailVC: GIDSignInUIDelegate, GIDSignInDelegate {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            // Perform any operations on signed in user here.
            appDelegate.showActivityIndicatory(uiView: self.view)
            let userId = user.userID                  // For client-side use only!
            let fullName = user.profile.name
            let email = user.profile.email
            self.gmailSignUP(name: fullName!, userIdAs: userId!, email: email!)
            
        } else {
            print("\(error.localizedDescription)")
            self.alert(message: error.localizedDescription)
        }
    }
    
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        myActivityIndicator.stopAnimating()
    }
    
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        print("print dissmiss the view controller")
        self.dismiss(animated: true, completion: nil)
    }
    //Sign out button to remove the data form user defaults
}


// MARK: - Login Api Extension
extension FaceBookGmailVC {
    func login(){
        
        appDelegate.showActivityIndicatory(uiView: self.view)
        let parametrs : [String : Any] = [
            "password": passwordTF.text!,
            "email": email.text!
        ]
        WebServices.loginApi(parametrs: parametrs, success: { (user) in
            self.appDelegate.hideActivityIndicatory()
            if user.statuss == "True" {
                self.saveUserDefault(user: user)
            } else {
                self.alert(message: user.message)
            }
        }, apifailuer: { (error) in
            self.appDelegate.hideActivityIndicatory()
            self.alert(message: error)
        }) { (error) in
            self.appDelegate.hideActivityIndicatory()
            self.alert(message: error)
        }
    }
    
    //MARK: - Gmail Sign Up -
    func gmailSignUP(name: String, userIdAs password: String, email: String) {
        setTextFiled(email: email, Password: password)
        let parametrs : [String : Any] = ["username" : name,
                                          "password": password,
                                          "country": "gmail.com",
                                          "phone": "gmail.com",
                                          "email": email
        ]
        WebServices.SignApi(parametrs: parametrs, success: { (user) in
            self.appDelegate.hideActivityIndicatory()
            if user.statuss == "True" {
                self.saveUserDefault(user: user)
            } else {
                if user.message == "Email Already Exist." {
                    self.login()
                } else {
                    self.alert(message: user.message)
                }
            }
        }, apifailuer: { (error) in
            self.appDelegate.hideActivityIndicatory()
            self.alert(message: error)
        }) { (error) in
            self.appDelegate.hideActivityIndicatory()
            self.alert(message: error)
        }
    }
}


