//
//  LeaderBordVC.swift
//  PSL
//
//  Created by Mudassar on 11/01/2018.
//  Copyright © 2018 HalfTech. All rights reserved.
//

import UIKit

class LeaderBordVC: UIViewController {

    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var leaderBord = LeaderBord()
    var refreshControl = UIRefreshControl()
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        callLeaderApi()
        setPullToRefresh()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Refrsh Controller -
    func setPullToRefresh() {
//        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: " ")
        refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: UIControlEvents.valueChanged)
        self.tableView.addSubview(refreshControl) // not required when using UITableViewController
    }
    
    @objc func handleRefresh(_: Any) {
        callLeaderApi()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
//MARK: - Table view Extension -
extension LeaderBordVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if leaderBord.statuss == "True" {
            return leaderBord.bordArray.count
        } else {
            // to load the pull to refresh cell
         return 1
        }
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if leaderBord.statuss == "True" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "LeaderBordTVC", for: indexPath) as! LeaderBordTVC
            let bordArry = leaderBord.bordArray[indexPath.row]
            cell.setData(leaderBord: bordArry)
            return cell
        } else {
            
            let cell = Bundle.main.loadNibNamed("PullToReFreshTVC", owner: self, options: nil)?.first as! PullToReFreshTVC
            
            return cell
        }
    }
}

//MARK: - Api Calling  -
extension LeaderBordVC {
    func callLeaderApi() {
        
        self.refreshControl.endRefreshing()
        appDelegate.showActivityIndicatory(uiView: self.view)
        WebServices.getLeaderBoard(success: { (leaderBoard) in
            self.appDelegate.hideActivityIndicatory()
            
            self.leaderBord = leaderBoard
            self.tableView.reloadData()
            
        }, apifailuer: { (error) in
            
            self.appDelegate.hideActivityIndicatory()
            self.alert(message: error)
            
        }) { (error) in
            
            self.appDelegate.hideActivityIndicatory()
            self.alert(message: error)
        }
    }
}


