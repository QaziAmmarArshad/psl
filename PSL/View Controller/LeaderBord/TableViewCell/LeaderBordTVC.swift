//
//  LeaderBordTVC.swift
//  PSL
//
//  Created by Mudassar on 11/01/2018.
//  Copyright © 2018 HalfTech. All rights reserved.
//

import UIKit

class LeaderBordTVC: UITableViewCell {

    @IBOutlet weak var rankLabel: UILabel!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var teamNameLbl: UILabel!
    @IBOutlet weak var pointsLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setData(leaderBord: BordArray)  {
        rankLabel.text = leaderBord.rank
        userNameLbl.text = leaderBord.username
        teamNameLbl.text = leaderBord.user_team
        pointsLbl.text = leaderBord.user_points
    }
    
}
