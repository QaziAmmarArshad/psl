//
//  MyTeamVC.swift
//  PSL
//
//  Created by Mudassar on 15/01/2018.
//  Copyright © 2018 HalfTech. All rights reserved.
//

import UIKit

class MyTeamVC: UIViewController {

    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var players = Players()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var transferLabel: UILabel!
    @IBOutlet weak var budgetLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.interactivePopGestureRecognizer?.delegate = self as? UIGestureRecognizerDelegate
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        // Do any additional setup after loading the view.
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        players = Players()
        cellSelectedPlayersApi()
    }

  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       
    }
    
    @IBAction func editButton(_ sender: Any) {
//        self.performSegue(withIdentifier: "editTeam", sender: self)
//        movetoSelectTeamVC
        movetoSelectTeamVC()
        
    }
    func movetoSelectTeamVC() {
        let selectTeam = storyboard?.instantiateViewController(withIdentifier: "SelectTeamVC") as! SelectTeamVC
        selectTeam.isHaveATeam = true
        navigationController?.pushViewController(selectTeam, animated: true)
    }
    
    @IBAction func backButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    //MARK: - User Define Function -
    func setBudgetTransfer() {
        budgetLabel.text = players.user_budget
        transferLabel.text = players.user_transfers
    }

}

extension MyTeamVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if players.statuss != "True" {
            return 0
        } else {
            return 4
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerview = Bundle.main.loadNibNamed("PlayerCategoryHeader", owner: self, options: nil)?.first as! PlayerCategoryHeader
        switch section {
        case 0:
             headerview.categoryLabel.text = "Bats Man"
        case 1:
             headerview.categoryLabel.text = "Bowler"
        case 2:
             headerview.categoryLabel.text = "All Rounder"
        default:
             headerview.categoryLabel.text = "Wicket Keeper"
        }
        return headerview
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return self.players.batsman.count
        case 1:
           return self.players.bowler.count
        case 2:
           return self.players.allrounder.count
        default:
            return self.players.wicketKeeper.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlayerDetailTVC", for: indexPath) as! PlayerDetailTVC
        switch indexPath.section {
        case 0:
            cell.setCellData(player: self.players.batsman[indexPath.row])
        case 1:
            cell.setCellData(player: self.players.bowler[indexPath.row])
        case 2:
            cell.setCellData(player: self.players.allrounder[indexPath.row])
        default:
            cell.setCellData(player: self.players.wicketKeeper[indexPath.row])
        }
        return cell
    }
}

extension MyTeamVC {
    
    func cellSelectedPlayersApi() {
        
        appDelegate.showActivityIndicatory(uiView: self.view)
        let parameters = ["user_id": UserDefaults.standard.string(forKey: Constant().UD_userId)!] as [String : Any]
        
        WebServices.getMyTeam(parameters: parameters, success: { (players) in
            
            self.appDelegate.hideActivityIndicatory()
            self.players = players
            self.setBudgetTransfer()
            self.tableView.reloadData()
            
        }, apifailuer: { (error) in
            
            self.appDelegate.hideActivityIndicatory()
            self.alert(message: error)
            
        }) { (error) in
            
            self.appDelegate.hideActivityIndicatory()
            self.alert(message: error)
        }
    }
}

