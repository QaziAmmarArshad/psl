//
//  PlayerDetailTVC.swift
//  PSL
//
//  Created by Mudassar on 15/01/2018.
//  Copyright © 2018 HalfTech. All rights reserved.
//

import UIKit
import SDWebImage

class PlayerDetailTVC: UITableViewCell {
    
    @IBOutlet weak var teamImage: UIImageView!
    @IBOutlet weak var playerName: UILabel!
    @IBOutlet weak var playerTeam: UILabel!
    @IBOutlet weak var value: UILabel!
    @IBOutlet weak var lastMatch: UILabel!
    @IBOutlet weak var totalPoints: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setCellData(player: SportMan) {
        playerName.text = player.name
        playerTeam.text = player.team_name
        value.text = player.budget
        lastMatch.text = player.budget
        totalPoints.text = player.total_points
//         teamImage.sd_setImage(with: URL(string: play), placeholderImage: UIImage(named: Constant().PLACE_HOLDER_Image) )
    }
    
}




