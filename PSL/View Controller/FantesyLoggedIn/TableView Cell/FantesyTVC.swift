//
//  FantesyTVC.swift
//  PSL
//
//  Created by Mudassar on 16/01/2018.
//  Copyright © 2018 HalfTech. All rights reserved.
//

import UIKit

class FantesyTVC: UITableViewCell {

    @IBOutlet weak var rankLbl: UILabel!
    @IBOutlet weak var leugeNameLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCellData(leader: Leader) {
        rankLbl.text = leader.rank
        leugeNameLbl.text = leader.league_name
    }
}


