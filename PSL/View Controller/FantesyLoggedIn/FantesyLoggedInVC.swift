//
//  FantesyLoggedInVC.swift
//  PSL
//
//  Created by Mudassar on 10/01/2018.
//  Copyright © 2018 HalfTech. All rights reserved.
//

import UIKit

class FantesyLoggedInVC: UIViewController {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var isHaveATeam = false
    var leugeName = ""
    var fantesy = Fantasy()
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var yourSocreLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        print(isHaveATeam)
        
        if !isHaveATeam {
//            self.performSegue(withIdentifier: Constant().S_selectTeamVC, sender: self)
            //move to selet team view controller
            moveToSelectTeamVC()
        }
        getFantasyApi()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func moveToSelectTeamVC() {
        
        self.performSegue(withIdentifier: "selectTeam", sender: self)

    }
    
    /* At start when user is login. he has no team so this force the user to first select the team and then move
        main screen */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.identifier == "selectTeam") {
            let selectTeamViewController = segue.destination as? SelectTeamVC
            selectTeamViewController?.isHaveATeam = false
            
        }      
    }
    
    @IBAction func moveToJoinLeugeVC(_ sender: Any) {
        let joinLeugeVC = storyboard?.instantiateViewController(withIdentifier: "JoinCreateLeugeVC") as! JoinCreateLeugeVC
        joinLeugeVC.isHaveAleuge = leugeName
        navigationController?.pushViewController(joinLeugeVC, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension FantesyLoggedInVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if fantesy.statuss == "True" {
            // returning 2 extra cell first for static header and second for overall ranking
           return fantesy.leader.count + 2
        } else  {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "FSPL Static")
            return cell!
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "FantesyTVC", for: indexPath) as! FantesyTVC
            cell.rankLbl.text = fantesy.OverAllRanking
            cell.leugeNameLbl.text = "Overall"
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "FantesyTVC", for: indexPath) as! FantesyTVC
            cell.setCellData(leader: fantesy.leader[indexPath.row - 2])
            return cell
        }
    }
    
}

//MARK: - Get Fantesy Api -
extension FantesyLoggedInVC {
    func getFantasyApi() {
        self.appDelegate.showActivityIndicatory(uiView: self.view)
        let parameter: [String: String] = ["user_id": UserDefaults.standard.string(forKey: Constant().UD_userId)!]

        WebServices.getFantasy(parameters: parameter, success: { (parseFantesy) in
            print(self.fantesy.OverAllRanking)
            self.fantesy = parseFantesy
            self.tableView.reloadData()
            self.appDelegate.hideActivityIndicatory()
        }, apifailuer: { (error) in
            self.appDelegate.hideActivityIndicatory()
            self.alert(message: error)
        }) { (error) in
            self.appDelegate.hideActivityIndicatory()
            self.alert(message: error)
        }
    }
}












