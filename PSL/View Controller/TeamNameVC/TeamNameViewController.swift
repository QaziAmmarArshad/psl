//
//  TeamNameViewController.swift
//  PSL
//
//  Created by Mudassar on 05/01/2018.
//  Copyright © 2018 HalfTech. All rights reserved.
//

import UIKit

class TeamNameViewController: UIViewController {

    @IBOutlet weak var teamName: UITextField!
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var selectedTeamId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        print(selectedTeamId)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK - Button Action -
    //back button to pop view controller form navigation controller
    
    @IBAction func sendDataToServer(_ sender: Any) {
        if (teamName.text?.isEmpty)! {
            self.alert(message: "Please enter Team Name")
        } else {
            self.callingApi()
        }
    }
    
    func moveToNextScreen() {
        let myTeamVC = storyboard?.instantiateViewController(withIdentifier: "FantesyLoggedInVC") as! FantesyLoggedInVC
        self.navigationController?.pushViewController(myTeamVC, animated: true)
    }
    @IBAction func backBtn(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
//MARK: - calling api services -
extension TeamNameViewController {
    func callingApi() {
        appDelegate.showActivityIndicatory(uiView: self.view)
        
        let parameters = ["user_id": UserDefaults.standard.string(forKey: Constant().UD_userId)!,
                          "team_id": selectedTeamId,
                          "team": self.teamName.text!] as [String : Any]
        
        WebServices.saveFavouriteTeam(parameters: parameters, success: { (message) in
            self.appDelegate.hideActivityIndicatory()
            if message == "Data has been updated." {
                self.moveToNextScreen()
            } else {
                self.alert(message: "message")
            }
            
        }, apifailuer: { (error) in
            
            self.appDelegate.hideActivityIndicatory()
            self.alert(message: error)
            
        }) { (error) in
            
            self.appDelegate.hideActivityIndicatory()
            self.alert(message: error)
        }
    }
}





