//
//  JoinCreateLeugeVC.swift
//  PSL
//
//  Created by Mudassar on 15/01/2018.
//  Copyright © 2018 HalfTech. All rights reserved.
//

import UIKit

class JoinCreateLeugeVC: UIViewController {
    
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var isHaveAleuge = ""
    
    //MARK: - IBoutLets -
    @IBOutlet weak var enterPinTF: UITextField!
    @IBOutlet weak var leugeNameTF: UITextField!
    @IBOutlet weak var leugeNameLbl: UILabel!
    @IBOutlet weak var createLeugeBtn: GreenButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self as? UIGestureRecognizerDelegate
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        // Do any additional setup after loading the view.
        print(isHaveAleuge)
        if !isHaveAleuge.isEmpty {
            hideViews()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    //MARK: - IBAction -
    @IBAction func joinLeugeBtn(_ sender: Any) {
        if !(enterPinTF.text?.isEmpty)! {
            joinLeagueApi()
        } else {
            self.alert(message: "Plese Enter Code")
        }
    }
    
    @IBAction func createLeugeBtn(_ sender: Any) {
        if !((leugeNameLbl.text?.isEmpty)!) {
            createLeugeApi(pin: getPin())
        }
    }
    
    @IBAction func backButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    //MARK: - User Define Function -
    
    func hideViews() {
        leugeNameLbl.text = "Your Leauge"
        createLeugeBtn.isHidden = true
        leugeNameTF.text = isHaveAleuge
        leugeNameTF.isUserInteractionEnabled = false
    }
    
    func getPin() -> String {
        var pin = ""
        let name = UserDefaults.standard.string(forKey: Constant().UD_Name) ?? ""
        let id =  UserDefaults.standard.string(forKey: Constant().UD_userId) ?? ""
        let currentDate = getCurrentDate()
        let nameIndex = name.index(name.startIndex, offsetBy: 1)
        let limitedName = name.prefix(upTo: nameIndex)
        pin = limitedName + id + currentDate
        return pin
    }
    
    func getCurrentDate() -> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "ddMMyy"
        return formatter.string(from: date)
    }
}

//MARK: -Api Extension -

extension JoinCreateLeugeVC {
    
    func createLeugeApi(pin: String) {
        self.appDelegate.showActivityIndicatory(uiView: self.view)
        
        let parameter = ["user_id": UserDefaults.standard.string(forKey: Constant().UD_userId)!,
                         "league_name": leugeNameLbl.text!,
                         "pin": pin] as [String : Any]
        
        WebServices.createLeague(parameters: parameter, success: { (status) in
            self.appDelegate.hideActivityIndicatory()
            if status == "True" {
                //show successfully created leuge
                self.alert(message: "Successfully Created leuge")
            } else {
                self.alert(message: "Request Fail")
            }
            
        }, apifailuer: { (error) in
            
            self.appDelegate.hideActivityIndicatory()
            self.alert(message: error)
        }) { (error) in
            
            self.appDelegate.hideActivityIndicatory()
            self.alert(message: error)
        }
    }
    
    func joinLeagueApi() {
        
        self.appDelegate.showActivityIndicatory(uiView: self.view)
        let parameter = ["user_id": UserDefaults.standard.string(forKey: Constant().UD_userId)!,
                         "pin": enterPinTF.text!] as [String : Any]
        
        WebServices.joinLeague(parameters: parameter, success: { (leauge) in
            self.appDelegate.hideActivityIndicatory()
            if leauge != "" {
                //show successfully Joined the leuge
                self.alert(message: leauge)
            } else {
                self.alert(message: "Request Fail")
            }
            
        }, apifailuer: { (error) in
            
            self.appDelegate.hideActivityIndicatory()
            self.alert(message: error)
        }) { (error) in
            
            self.appDelegate.hideActivityIndicatory()
            self.alert(message: error)
        }
    }
}






