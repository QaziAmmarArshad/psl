//
//  Help.swift
//  PSL
//
//  Created by Mudassar on 16/01/2018.
//  Copyright © 2018 HalfTech. All rights reserved.
//

import Foundation
import SwiftyJSON

class Help: Response {
    var help = ""
    var rules = [Rules]()
    
    override init() {
    }
    
    init(json: JSON) {
        super.init()
        statuss = json["statuss"].string ?? ""
        message = json["message"].string ?? ""
        help = json["Help"].string ?? ""
        
        let ruleJsonArray = json["Rules"].array ?? []
        for jsonRule in ruleJsonArray {
            rules.append(Rules(json: jsonRule))
        }
    }
}

class Rules {
    var rule: String
    var point: String
    
    init(json: JSON) {
        rule = json["rule"].string ?? ""
        point = json["point"].string ?? ""
    }
}





