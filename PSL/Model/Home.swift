//
//  Home.swift
//  PSL
//
//  Created by Mudassar on 05/01/2018.
//  Copyright © 2018 HalfTech. All rights reserved.
//

import Foundation
import SwiftyJSON

class Home: Response{
    
    var fixtures = [Fixtures]()
    var results = [Results]()
    var points_Tables = [Points_Table]()
    
    override init() {
        super.init()
    }
    
    init(json: JSON) {
        super.init()
        let response = json["Response"]
        self.statuss = response["statuss"].string ?? "False"
        self.message = response["message"].string ?? ""
        if statuss == "True"
        {
            let fixturesArray = response["Fixtures"].array ?? []
            let resultArray = response["Results"].array ?? []
            let pointsTableArray = response["Points_Table"].array ?? []
            
            for fixture in fixturesArray {
                let tempFixture = Fixtures(json: fixture)
                fixtures.append(tempFixture)
            }
            for result in resultArray {
                let tempResult = Results(json: result)
                results.append(tempResult)
            }
            for point in pointsTableArray {
                let tempPoint = Points_Table(json: point)
                points_Tables.append(tempPoint)
            }
            
        }
    }
    
}

class Fixtures {
    var matchdate: String = ""
    var matches = [Matches]()
    init(json: JSON) {
        matchdate = json["matchdate"].string ?? ""
        let matchsArray = json["Matches"].array ?? []
        for match in matchsArray {
            let tempMatch = Matches(json: match)
            matches.append(tempMatch)
        }
    }
}

class Matches {
    //use in fixtures class
    var time: String = ""
    var team1: String = ""
    var team2: String = ""
    var team1Detail = TeamDetail()
    var team2Detail = TeamDetail()
    init(json: JSON) {
        time = json["time"].string ?? ""
        team1 = json["team1"].string ?? ""
        team2 = json["team2"].string ?? ""
        team1Detail = TeamDetail(json: json["team_1_detail"])
        team2Detail = TeamDetail(json: json["team_2_detail"])
    }
}

class TeamDetail {
    //use in macthce calss
    //use in result calss
    var team_id: String!
    var logo: String!
    var name: String!
    var nick_name: String!
    init() {
    }
    init(json: JSON) {
        team_id = json["team_id"].string ?? ""
        name = json["name"].string ?? ""
        logo = json["logo"].string ?? ""
        nick_name = json["nick_name"].string ?? ""
    }
}

class Results {
    var matchdate: String = ""
    var match_results = [MatchResults]()
    init(json: JSON) {
          matchdate = json["matchdate"].string ?? ""
         let jsonArray_match_results = json["match_result"].array ?? []
        
        for jsonObj_match_result in jsonArray_match_results {
            match_results.append(MatchResults(json: jsonObj_match_result))
        }
    }
}

class MatchResults {
    var match_id: String = ""
    var won_team: String = ""
    var loose_team: String = ""
    var won_team_score: String = ""
    var loose_team_score: String = ""
    var won_team_detail: TeamDetail!
    var loose_team_detail: TeamDetail!
    init() {
    }
    init(json: JSON) {
        match_id = json["match_id"].string ?? ""
        won_team = json["won_team"].string ?? ""
        loose_team = json["loose_team"].string ?? ""
        won_team_score = json["won_team_score"].string ?? ""
        loose_team_score = json["loose_team_score"].string ?? ""
        won_team_detail = TeamDetail(json: json["won_team_detail"])
        loose_team_detail = TeamDetail(json: json["loose_team_detail"])
    }
}

class Points_Table {
    
    var team_id: String = ""
    var logo: String = ""
    var name: String = ""
    var nick_name: String = ""
    var W: String = ""
    var P: String = ""
    var L: String = ""
    var nrr: String = ""
    
    init(json: JSON) {
        team_id = json["team_id"].string ?? ""
        logo = json["logo"].string ?? ""
        name = json["name"].string ?? ""
        nick_name = json["nick_name"].string ?? ""
        W = json["W"].string ?? ""
        P = json["P"].string ?? ""
        L = json["L"].string ?? ""
        nrr = json["nrr"].string ?? ""
    }
    
}





