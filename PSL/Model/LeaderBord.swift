//
//  LeaderBord.swift
//  PSL
//
//  Created by Mudassar on 11/01/2018.
//  Copyright © 2018 HalfTech. All rights reserved.
//

import Foundation
import SwiftyJSON

class LeaderBord: Response {
    var bordArray = [BordArray]()
    override init() {}
    init(json: JSON) {
        super.init()
        statuss = json["statuss"].string ?? ""
        message = json["message"].string ?? ""
        let jsonLeaderBordArray = json["LeaderBoard"].array ?? []
        
        for jsonLeaderBord in jsonLeaderBordArray {
            bordArray.append(BordArray(json: jsonLeaderBord))
        }
    }
}

class BordArray {
    
    var user_id = ""
    var user_team = ""
    var username = ""
    var user_points = ""
    var rank = ""
    
    init(json: JSON) {
        user_id = json["user_id"].string ?? ""
        user_team = json["user_team"].string ?? ""
        username = json["username"].string ?? ""
        user_points = json["user_points"].string ?? ""
        rank = json["rank"].string ?? ""
    }
    
}
