//
//  User.swift
//  PSL
//
//  Created by Mudassar on 09/01/2018.
//  Copyright © 2018 HalfTech. All rights reserved.
//

import Foundation
import SwiftyJSON

class User: Response {
    var user_id: String = ""
    var token: String = ""
    var name: String = ""
    var email: String = ""
    var user_team = ""
    var user_favourite_team = ""
    var selected_team = false
    var create_league = ""
    
     init(json: JSON) {
        super.init()
        let response = json["Response"]
        self.statuss = response["statuss"].string ?? "False"
        self.message = response["message"].string ?? ""
        if statuss == "True" {
            let data = response["Data"]
            user_id = data["user_id"].string ?? ""
            token = data["token"].string ?? ""
            name = data["name"].string ?? ""
            email = data["email"].string ?? ""
            user_team = data["user_team"].string ?? ""
            user_favourite_team = data["user_favourite_team"].string ?? ""
             let isHaveATeam = data["selected_team"].string ?? "False"
            if isHaveATeam == "True" {
                selected_team = true
            }
            create_league = data["create_league"].string ?? ""
        }
    }
}




