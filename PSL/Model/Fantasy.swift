//
//  Fantasy.swift
//  PSL
//
//  Created by Mudassar on 16/01/2018.
//  Copyright © 2018 HalfTech. All rights reserved.
//

import Foundation
import SwiftyJSON

// This model is use in get Fantesy page
class Fantasy: Response {
    
    var Total_Point = ""
    var OverAllRanking = ""
    var leader = [Leader]()
    override init() {
    }
    init(json: JSON) {
        super.init()
        statuss = json["statuss"].string ?? "False"
        message = json["message"].string ?? "Request Fail"
        Total_Point = json["Total_Point"].string ?? ""
        OverAllRanking = json["OverAllRanking"].string ?? "No"
        let leaderJsonArray = json["leader"].array ?? []
        
        for leaderJson in leaderJsonArray {
            let leaderObj = Leader(json: leaderJson)
            leader.append(leaderObj)
        }
    }
}

class Leader {
    var league_id = ""
    var league_name = ""
    var rank = ""
    init() {
    }
    init(json: JSON) {
        league_id = json["league_id"].string ?? ""
        league_name = json["league_name"].string ?? ""
        rank = json["rank"].string ?? ""
    }
}






