//
//  FixturesAndResults.swift
//  PSL
//
//  Created by Mudassar on 16/01/2018.
//  Copyright © 2018 HalfTech. All rights reserved.
//

import Foundation
import SwiftyJSON

class FixturesAndResults: Response {
    var fixturesResults = [FixturesResults]()
    override init() {
        
    }
    init(json: JSON) {
        super.init()
        let response = json["Response"]
        statuss = response["statuss"].string ?? "False"
        message = response["message"].string ?? ""
        if statuss == "True" {
            let fixturesjsonArray = response["FixturesAndResults"].array ?? []
            
            for jsonFixture in fixturesjsonArray {
                fixturesResults.append(FixturesResults(json: jsonFixture))
            }
            
        }
    }
}

class FixturesResults: Fixtures {
    var results = [MatchResults]()
    
    override init(json: JSON) {
        super.init(json: json)

        let jsonResultArray = json["result"].array ?? []
        for result in jsonResultArray {
            let tempResult = MatchResults(json: result)
            results.append(tempResult)
        }
    }
    
}
















