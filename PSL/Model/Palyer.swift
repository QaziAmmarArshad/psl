//
//  GetAllPalyer.swift
//  PSL
//
//  Created by Mudassar on 11/01/2018.
//  Copyright © 2018 HalfTech. All rights reserved.
//

import Foundation
import SwiftyJSON

class Players: Response {
    var user_budget = ""
    var user_transfers = ""
    var batsman = [SportMan]()
    var bowler = [SportMan]()
    var wicketKeeper = [SportMan]()
    var allrounder = [SportMan]()
    
    override init() {
        
    }
    
    init(json: JSON)  {
        super.init()
        statuss = json["statuss"].string ?? "False"
        message = json["message"].string ?? ""
        
        let jsonPlayer = json["Players"]
        user_budget = jsonPlayer["user_budget"].string ?? ""
        user_transfers = jsonPlayer["user_transfers"].string ?? ""
        
        let batsmanJsonArray  = jsonPlayer["Batsman"].array ?? []
        let bowlerJsonArray = jsonPlayer["Bowler"].array ?? []
        let wicketKeeperJsonArray = jsonPlayer["WicketKeeper"].array ?? []
        let allrounderJsonArray = jsonPlayer["Allrounder"].array ?? []
        
        for batmanJson in batsmanJsonArray {
            batsman.append(SportMan(from: batmanJson))
        }
        for bowlerJson in bowlerJsonArray {
            bowler.append(SportMan(from: bowlerJson))
        }
        for wicketKeeperJson in wicketKeeperJsonArray {
            wicketKeeper.append(SportMan(from: wicketKeeperJson))
        }
        for allrounderJson in allrounderJsonArray {
            allrounder.append(SportMan(from: allrounderJson))
        }
    }
    
}

class SportMan: NSObject {
    
    var player_id = ""
    var name = ""
    var budget = ""
    var total_points = ""
    var category_id  = ""
    var team_name = ""
    var isSelected = false
    var logo = ""
    var captain = ""
    var vice_captain = ""
    var last_match_points = ""
    
    override init() {
    }
    init(from json: JSON) {
        player_id = json["player_id"].string ?? ""
        name = json["player_name"].string ?? ""
        budget = json["budget"].string ?? ""
        total_points = json["total_points"].string ?? ""
        category_id = json["category_id"].string ?? ""
        team_name = json["team_name"].string ?? ""
        let selected = json["is_selected"].string ?? "0"
        if selected == "1" { isSelected = true}
        logo = json["logo"].string ?? ""
        captain = json["captain"].string ?? ""
        vice_captain = json["vice_captain"].string ?? ""
        last_match_points = json["last_match_points"].string ?? ""
        
    }
}






