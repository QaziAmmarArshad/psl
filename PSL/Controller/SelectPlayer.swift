//
//  SelectPlayer.swift
//  PSL
//
//  Created by Mudassar on 12/01/2018.
//  Copyright © 2018 HalfTech. All rights reserved.
//

import Foundation


class DateController {
    
   class func setDate(string_date: String) -> String {
        // setting date formate to Swift formate
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-mm-dd" //Your date format
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00") //Current time zone
        let date = dateFormatter.date(from: string_date) //according to date format your date string
        dateFormatter.dateStyle = .long
        let newDate = dateFormatter.string(from: date!) //pass Date here
        return newDate
    }
}





