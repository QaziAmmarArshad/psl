//
//  FaceBookLogin.swift
//  PSL
//
//  Created by Mudassar on 04/01/2018.
//  Copyright © 2018 HalfTech. All rights reserved.
//

import Foundation
import FacebookLogin
import FacebookCore

protocol FacebookDelegate {
    func facebookSuccess(dictionaryValue: [String: Any])
    func facebookFailuer(error: String)
}

class FacebookLogin {
    
    var delegate: FacebookDelegate?
    let appdelegate = UIApplication.shared.delegate as! AppDelegate
    
    func login(viewController: UIViewController){
        appdelegate.showActivityIndicatory(uiView: viewController.view)
        
        if let accessToken = AccessToken.current {
            // User is logged in, use 'accessToken' here.
            print("accessToken is present")
            print(accessToken.userId!)
            getFaceBookUserData(accessToken: accessToken)
        } else {
            // User is not logged in, first 'accessToken' for token and then use 'accessToken' here.
            self.requestAccessToken(viewController: viewController)
        }
    }
    
    //MARK: Get faceBook Access Token
    private func requestAccessToken(viewController: UIViewController) {
        let loginManager = LoginManager()
        loginManager.logIn(readPermissions: [.publicProfile,.email], viewController: viewController) { loginResult in
            switch loginResult {
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success(let grantedPermissions, let declinedPermissions, let accessToken):
                print(grantedPermissions)
                print(declinedPermissions)
                //this call function to get user data
                self.getFaceBookUserData(accessToken: accessToken)
                print("Logged in!")
            }
        }
    }
    
    //MARK: request to get user request form facebook
    private func getFaceBookUserData(accessToken: AccessToken) {
        let request = GraphRequest(graphPath: accessToken.userId!, parameters: ["fields" : "name,email,link"],httpMethod: .GET)
        request.start { (httpResponse, result) in
            switch result {
            case .success(let response):
                let responseDictionary = response.dictionaryValue
                print(responseDictionary!)
                self.delegate?.facebookSuccess(dictionaryValue: responseDictionary!)
//                https://graph.facebook.com/1261242187309897/picture?type=large
                //                let profile = UserProfile(userId: userId,
                //                                         firstName: responseDictionary?["first_name"] as? String,
                //                                         middleName: responseDictionary?["middle_name"] as? String,
                //                                         lastName: responseDictionary?["last_name"] as? String,
                //                                         fullName: responseDictionary?["name"] as? String,
                //                                         profileURL: (responseDictionary?["link"] as? String).flatMap({ URL(string: $0) }),
            //                                         refreshDate: Date())
            case .failed(let error):
                print(error)
                self.delegate?.facebookFailuer(error: error.localizedDescription)
            }
        }
    }
}
