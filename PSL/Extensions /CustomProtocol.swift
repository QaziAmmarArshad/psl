//
//  CustomProtocol.swift
//  PSL
//
//  Created by Mudassar on 17/01/2018.
//  Copyright © 2018 HalfTech. All rights reserved.
//

import Foundation
protocol SaveTeam {
    func sendDateToServer(captainId: String, viceCaptainId: String)
}
