//
//  UIElements.swift
//  PSL
//
//  Created by Mudassar on 02/01/2018.
//  Copyright © 2018 HalfTech. All rights reserved.
//

import Foundation
import UIKit

//create custom green button with rounder corner
class GreenButton: UIButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor(red: 1.0/255.0, green: 149.0/255.0, blue: 37.0/255.0, alpha: 1.0)
        self.layer.cornerRadius = 5
//        self.titleLabel?.textColor = UIColor.white
        self.setTitleColor(UIColor.white, for: .normal)
//        self.titleLabel?.font = UIFont(name: "Brown Bold", size: 12)
    }
}
//make round the corner of the button
class RoundedButton: UIButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = 5
    }
}
//make the view circle
class RoundedView: UIView {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = self.frame.width
    }
}
