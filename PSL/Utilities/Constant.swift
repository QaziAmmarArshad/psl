//
//  Constant.swift
//  PSL
//
//  Created by Mudassar on 01/01/2018.
//  Copyright © 2018 HalfTech. All rights reserved.
//

import Foundation
class Constant {
    //MARK: Cell Identifiers
    static let CURRENTMATCH_CELL = "currentMactch"
    static let UPCOMINGFIXTURES_CELL = "UpcomingFixtures"
    static let LEUGELIST_CELL = "LeagueList"
    static let LEUGETOP_CELL = "LeagTop"
    let Match_Between_Xib = "MatchBetween"
    let Player_Category_Header_Xib = "PlayerCategoryHeader"
    let PLACE_HOLDER_Image = "Placeholder"
    let CELL_SelectPlayer = "SelectPlayerTVC"
    
    //MARK: - User Defaults Constances -
    let UD_userId = "user_id"
    let UD_Email = "email"
    let UD_Token = "token"
    let UD_Name = "name"
    let UD_Password = "password"
    
    //MARK: - Seuge Constants -
    let S_selectTeamVC = "selectTeam"
    
}





