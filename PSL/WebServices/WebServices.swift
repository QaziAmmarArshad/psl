//
//  WebServices.swift
//  PSL
//
//  Created by Mudassar on 05/01/2018.
//  Copyright © 2018 HalfTech. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

typealias HomeSuccess = (_ parseObj: Home) -> Void
typealias UserSuccess = (_ parseObj: User) -> Void
typealias GetAllTeamsSuccess = (_ parseObj: GetAllTeam) -> Void
typealias GetAllPlayerSuccess = (_ parseObjL: Players) -> Void
typealias GetLeaderBordSuccess = (_ parseObjL: LeaderBord) -> Void
typealias GetFantesySuccess = (_ parseObj: Fantasy) -> Void
typealias GetHeplSuccess = (_ parseObj: Help) -> Void
typealias GetFixturesAndResults = (_ parseObj: FixturesAndResults) -> Void
typealias ApiFailuer = (_ errorString : String) -> Void
typealias HTTPfailuer = (_ errorString : String) -> Void

class WebServices {
    
    static let baseUrl = "http://home-arc.com/psl/index.php/Book/"
    static let loginBaseUrl = "http://home-arc.com/psl/index.php/Auth/"
    static let headers: HTTPHeaders = [ "Client-Service" : "pslhalftechownpr-client","Auth-Key":"PslAndroidIoSapIsScured!@)(@"]
    static var fullHeader = [String: String]()
    
    // MARK: - calling home api -
    class func homeApi(success: @escaping HomeSuccess, apifailuer: @escaping ApiFailuer, httpFailuer: @escaping HTTPfailuer){
        let url = baseUrl + "getHomepage"
        Alamofire.request(url, method: .post, parameters: nil, encoding: URLEncoding.default, headers: headers).validate().responseJSON { (respones) in
            switch respones.result {
            case .success(let data):
                let homeJson = JSON(data)
                let home = Home(json: homeJson)
                success(home)
                
            case .failure(let error):
                print(error.localizedDescription)
                apifailuer(error.localizedDescription)
            }
        }
    }
    
    // MARK: - calling Sign Up -
    class func SignApi(parametrs: [String : Any], success: @escaping UserSuccess, apifailuer: @escaping ApiFailuer, httpFailuer: @escaping HTTPfailuer){
        
        let url = loginBaseUrl + "signup"
        Alamofire.request(url, method: .post, parameters: parametrs, encoding: URLEncoding.default, headers: headers).validate().responseJSON { (respones) in
            switch respones.result {
            case .success(let data):
                
                let user = User(json: JSON(data))
                print(user.statuss)
                print(user.message)
                success(user)
                
            case .failure(let error):
                
                print(error.localizedDescription)
                apifailuer(error.localizedDescription)
            }
        }
    }
    
    // MARK: - calling login Up -
    class func loginApi(parametrs: [String : Any], success: @escaping UserSuccess, apifailuer: @escaping ApiFailuer, httpFailuer: @escaping HTTPfailuer){
        
        let url = loginBaseUrl + "login"
        Alamofire.request(url, method: .post, parameters: parametrs, encoding: URLEncoding.default, headers: headers).validate().responseJSON { (respones) in
            switch respones.result {
            case .success(let data):
                let user = User(json: JSON(data))
                success(user)
            case .failure(let error):
                
                print(error.localizedDescription)
                apifailuer(error.localizedDescription)
            }
        }
    }
    
    // MARK: - calling Get All Teams api -
    class func getAllTeams(success: @escaping GetAllTeamsSuccess, apifailuer: @escaping ApiFailuer, httpFailuer: @escaping HTTPfailuer){
        let url = baseUrl + "getAllTeams"
         updateHeader()
        Alamofire.request(url, method: .post, parameters: nil, encoding: URLEncoding.default, headers: fullHeader).validate().responseJSON { (respones) in
            switch respones.result {
            case .success(let data):
                
                let json = JSON(data)
                let allTeam = GetAllTeam(json: json)
                success(allTeam)
                
            case .failure(let error):
                print(error.localizedDescription)
                apifailuer(error.localizedDescription)
            }
        }
    }
    
    //MARK: - Save favoutire Team -
    class func saveFavouriteTeam(parameters: [String: Any], success: @escaping (_ parseObj: String) -> Void, apifailuer: @escaping ApiFailuer, httpFailuer: @escaping HTTPfailuer ) {
        
        let url = baseUrl + "saveFavouriteTeam"
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: fullHeader).validate().responseJSON { (response) in
            switch response.result {
            case .success(let data):
                print(data)
                let json = JSON(data)
                let response = json["Response"]
                let message = response["message"].string ?? "False"
                success(message)
            case .failure(let error):
                print(error.localizedDescription)
                apifailuer(error.localizedDescription)
            }
        }
    }
    
    //MARK: - Get All Players -
    class func getAllPlayers(parameters: [String: Any], success: @escaping GetAllPlayerSuccess , apifailuer: @escaping ApiFailuer, httpFailuer: @escaping HTTPfailuer ) {

        updateHeader()
        
        let url = baseUrl + "getAllPlayers"
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: fullHeader).validate().responseJSON { (response) in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                let player = Players(json: json["Response"])
                success(player)
                
            case .failure(let error):
                print(error.localizedDescription)
                apifailuer(error.localizedDescription)
            }
        }
    }
    
    // MARK: - calling Get Leader Board api -
    class func getLeaderBoard(success: @escaping GetLeaderBordSuccess, apifailuer: @escaping ApiFailuer, httpFailuer: @escaping HTTPfailuer){
        let url = baseUrl + "getLeaderBoard"
        
        Alamofire.request(url, method: .post, parameters: nil, encoding: URLEncoding.default, headers: headers).validate().responseJSON { (respones) in
            switch respones.result {
            case .success(let data):
                
                let json = JSON(data)
                let leaderbord = LeaderBord(json: json["Response"])
                success(leaderbord)
            case .failure(let error):
                
                print(error.localizedDescription)
                apifailuer(error.localizedDescription)
            }
        }
    }
    
    //MARK: - Save Selected Team -
    class func saveSelectedTeam(parameters: [String: Any], success: @escaping (_ message: String) -> Void   , apifailuer: @escaping ApiFailuer, httpFailuer: @escaping HTTPfailuer ) {
        
        updateHeader()
        
        let url = baseUrl + "saveSelectedTeam"
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: fullHeader).validate().responseJSON { (response) in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                let response = json["Response"]
                let message = response["statuss"].string ?? "False"
                success(message)
            case .failure(let error):
                print(error.localizedDescription)
                apifailuer(error.localizedDescription)
            }
        }
    }
    //MARK: - Save updateTeam Team -
    class func updateTeam(parameters: [String: Any], success: @escaping (_ message: String) -> Void   , apifailuer: @escaping ApiFailuer, httpFailuer: @escaping HTTPfailuer ) {
        
        updateHeader()
        
        let url = baseUrl + "updateTeam"
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: fullHeader).validate().responseJSON { (response) in
            switch response.result {
            case .success(let data):
                print(data)
                let json = JSON(data)
                let response = json["Response"]
                let message = response["statuss"].string ?? "False"
                success(message)
            case .failure(let error):
                print(error.localizedDescription)
                apifailuer(error.localizedDescription)
            }
        }
    }
    
    //MARK: - Halp & Rule Team -
    class func getHelpPage(success: @escaping GetHeplSuccess, apifailuer: @escaping ApiFailuer, httpFailuer: @escaping HTTPfailuer ) {
        
        updateHeader()
        let url = baseUrl + "getHelpPage"
        Alamofire.request(url, method: .post, parameters: nil, encoding: URLEncoding.default, headers: fullHeader).validate().responseJSON { (response) in
            switch response.result {
            case .success(let data):
                
                let json = JSON(data)
                let response = json["Response"]
                let help = Help(json: response)
                success(help)
                
            case .failure(let error):
                print(error.localizedDescription)
                apifailuer(error.localizedDescription)
            }
        }
    }
    
    //MARK: - Get Selected Players -
    class func getSelectedPlayers(parameters: [String: Any], success: @escaping GetAllPlayerSuccess , apifailuer: @escaping ApiFailuer, httpFailuer: @escaping HTTPfailuer ) {
        
        updateHeader()
        
        let url = baseUrl + "getSelectedPlayers"
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: fullHeader).validate().responseJSON { (response) in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                let player = Players(json: json["Response"])
                success(player)
                
            case .failure(let error):
                print(error.localizedDescription)
                apifailuer(error.localizedDescription)
            }
        }
    }
    //MARK: - Get Fantasy Api -
    class func getFantasy(parameters: [String: Any], success: @escaping GetFantesySuccess, apifailuer: @escaping ApiFailuer, httpFailuer: @escaping HTTPfailuer ) {
        
        updateHeader()
        let url = baseUrl + "getFantasy"
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: fullHeader).validate().responseJSON { (response) in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                let player = Fantasy(json: json["Response"])
                success(player)
                
            case .failure(let error):
                print(error.localizedDescription)
                apifailuer(error.localizedDescription)
            }
        }
    }
    
    //MARK: - Get FixturesAndResults Api -
    class func getFixturesAndResults(parameters: [String: Any], success: @escaping GetFixturesAndResults, apifailuer: @escaping ApiFailuer, httpFailuer: @escaping HTTPfailuer ) {
        
        updateHeader()
        let url = baseUrl + "getFixturesAndResults"
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: fullHeader).validate().responseJSON { (response) in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                let fixturesAndResults = FixturesAndResults(json: json)
                success(fixturesAndResults)
                
            case .failure(let error):
                print(error.localizedDescription)
                apifailuer(error.localizedDescription)
            }
        }
    }
    
    //MARK: - Get MyTeam Api -
    class func getMyTeam(parameters: [String: Any], success: @escaping GetAllPlayerSuccess, apifailuer: @escaping ApiFailuer, httpFailuer: @escaping HTTPfailuer ) {
        
        updateHeader()
        let url = baseUrl + "getMyTeam"
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: fullHeader).validate().responseJSON { (response) in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                let player = Players(json: json["Response"])
                success(player)
                
            case .failure(let error):
                print(error.localizedDescription)
                apifailuer(error.localizedDescription)
            }
        }
    }
    
    //MARK: -  Create League Api -
    class func createLeague(parameters: [String: Any], success: @escaping (_ message: String) -> Void, apifailuer: @escaping ApiFailuer, httpFailuer: @escaping HTTPfailuer ) {
        
        updateHeader()
        let url = baseUrl + "createLeague"
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: fullHeader).validate().responseJSON { (response) in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                let response = json["Response"]
                let message = response["statuss"].string ?? "False"
                success(message)
                
            case .failure(let error):
                print(error.localizedDescription)
                apifailuer(error.localizedDescription)
            }
        }
    }
    
    //MARK: -  joinLeague Api -
    class func joinLeague(parameters: [String: Any], success: @escaping (_ message: String) -> Void, apifailuer: @escaping ApiFailuer, httpFailuer: @escaping HTTPfailuer ) {
        
        updateHeader()
        let url = baseUrl + "joinLeague"
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: fullHeader).validate().responseJSON { (response) in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                let response = json["Response"]
                let statuss = response["statuss"].string ?? "False"
                if statuss == "False" {
                    success(response["message"].string ?? "")
                } else {
                    let name = response["league_name"].string ?? ""
                    success("successfully joined" + name )
                }
                
            case .failure(let error):
                print(error.localizedDescription)
                apifailuer(error.localizedDescription)
            }
        }
    }
    
    //MARK - After Login or sign Up update header
    class func updateHeader() {
        fullHeader = [ "Client-Service" : "pslhalftechownpr-client","Auth-Key":"PslAndroidIoSapIsScured!@)(@",
                       "Auty": UserDefaults.standard.string(forKey: Constant().UD_Token)!]
    }
}
